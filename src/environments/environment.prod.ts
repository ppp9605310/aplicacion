/**
 * Pone en producción la aplicación.
 */
export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyDxKCI4NsPwemIMnkkgGx1kgrmZFD23GXk',
    authDomain: 'ionicfirebase-11be8.firebaseapp.com',
    databaseURL:
      'https://ionicfirebase-11be8-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'ionicfirebase-11be8',
    storageBucket: 'ionicfirebase-11be8.appspot.com',
    messagingSenderId: '1075018770032',
    appId: '1:1075018770032:web:0a384eadfcff08237bf6b5',
    measurementId: 'G-X9JBW696P1',
  },
  news: {
    apiKey: '392c4ef2447040d1aab42bffac29c335',
    apiUrl: 'https://newsapi.org/v2',
  },
};
