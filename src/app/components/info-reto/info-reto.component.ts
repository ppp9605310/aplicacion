// Importar los módulos necesarios
import { Component, Input, OnInit } from '@angular/core';

import { ModalController, Platform } from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

import { Reto } from 'src/app/interfaces/interfaces';
import { RetoService } from 'src/app/services/reto.service';
import { AvisosService } from 'src/app/services/avisos.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

/**
 * Componente que muestra un ion-card con toda la información y las actividades que se pueden realizar con cada reto.
 * Este componente se muestra como una tarjeta con el título, el tipo, el nivel, la descripción y el botón de compartir del reto.
 * Al pulsar el botón de compartir, se abre el plugin de SocialSharing, que permite compartir el reto por diferentes redes sociales o aplicaciones.
 * Este componente se usa en las páginas de retos, favoritos y logros, donde se muestran los retos disponibles, los retos favoritos y los retos conseguidos por el usuario, respectivamente.
 */

@Component({
  selector: 'app-info-reto',
  templateUrl: './info-reto.component.html',
  styleUrls: ['./info-reto.component.scss'],
})
export class InfoRetoComponent implements OnInit {
  /**
   * Propiedad de entrada que recibe el id del reto a través de un input que se envía desde el componente padre.
   * @type {string} El id del reto
   */
  @Input() retoId: string;

  /**
   * Propiedad que almacena el objeto reto con toda la información a mostrar.
   * @type {Reto} El objeto reto
   */
  reto: Reto;

  /**
   * Propiedad que almacena el número de caracteres a mostrar de la descripción del reto.
   * Por defecto es 150, pero se puede cambiar al pulsar el botón de mostrar más o mostrar menos.
   * @type {number} El número de caracteres
   */
  oculto = 150;

  /**
   * Propiedad que almacena el mensaje estándar para el botón de compartir en redes sociales.
   * @type {string} El mensaje
   */
  mensaje: string = 'Te reto!';

  /**
   * Propiedad que almacena el email del usuario logado.
   * @type {string} El email
   */
  email: string;

  /**
   * Propiedad que almacena un valor booleano que indica si el reto es favorito o no para el usuario logado.
   * @type {boolean} El valor
   */
  esFavorito: boolean;

  /**
   * Propiedad que almacena un valor booleano que indica si el reto es conseguido o no por el usuario logado.
   * @type {boolean} El valor
   */
  esRetoConseguido: boolean;

  /**
   * Constructor de la clase
   * @param retoSvc Servicio que maneja la colección 'retos' de Firestore, que permite obtener, crear, modificar y eliminar retos
   * @param modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
   * @param socialSharing Plugin de Cordova que permite compartir contenido por diferentes redes sociales o aplicaciones
   * @param avisosSvc Servicio que muestra avisos de éxito o error al usuario mediante toast o alert
   * @param authSvc Servicio que controla el inicio de sesión y el registro de los usuarios mediante Firebase Authentication
   * @param userSvc Servicio que maneja la colección 'users' de Firestore, que permite obtener, crear, modificar y eliminar usuarios
   * @param platform Servicio de Ionic que permite acceder a la información de la plataforma donde se ejecuta la aplicación
   */

  constructor(
    private retoSvc: RetoService,
    private modalCtrl: ModalController,
    private socialSharing: SocialSharing,
    private avisosSvc: AvisosService,
    private authSvc: AuthService,
    private userSvc: UserService,
    private platform: Platform
  ) {}

  /**
   * Método de inicio que se ejecuta cuando el componente se inicializa.
   * Obtiene el email del usuario logado, el objeto reto con el id pasado por parámetro a través del input, y comprueba si el reto es favorito o conseguido por el usuario.
   */
  ngOnInit() {
    // Obtener el email del usuario logado
    this.authSvc.getUserEmail().then((email) => {
      this.email = email;
    });

    // Obtener el objeto reto con el id pasado por parámetro a través del input
    this.retoSvc.getRetosById(this.retoId).subscribe((retos) => {
      // Guardar el primer elemento del array de retos, que es el que coincide con el id
      this.reto = retos[0];

      // Comprobar si el reto es favorito o conseguido por el usuario
      this.checkRetoFavorito(this.reto.ID, this.email);
      this.checkRetoConseguido(this.reto.ID, this.email);
    });
  }

  /**
   * Método que asigna un icono dependiendo del tipo de reto pasado por parámetro.
   * @param tipo El tipo de reto
   * @returns {string} El nombre del icono de ionicons.io
   */
  getIconTipo(tipo: string): string {
    // Usar un switch para asignar el icono según el tipo de reto
    switch (tipo) {
      case 'telescopio':
        return 'telescope-outline';
      case 'prismaticos':
        return 'recording-outline';
      case 'ocular':
        return 'eye-outline';
      default:
        return 'help';
    }
  }

  /**
   * Método que asigna un color al icono dependiendo del nivel de dificultad del reto
   * @param nivel Nivel de dificultad del Reto
   * @returns Retorna el color a poner en el icono
   */
  getColorNivel(nivel: string): string {
    switch (nivel) {
      case 'facil':
        return 'success';
      case 'intermedio':
        return 'warning';
      case 'dificil':
        return 'danger';
      default:
        return 'help';
    }
  }

  /**
   * Método para compartir el reto en las aplicaciones disponibles en el terminal (Mail o RRSS).
   * Este método usa el plugin de SocialSharing, que permite compartir contenido por diferentes redes sociales o aplicaciones.
   * Para que funcione el plugin, se debe cumplir una de estas condiciones:
   * - La plataforma debe ser Android, ya que el plugin solo funciona en dispositivos Android.
   * - La plataforma debe soportar la API de navegador share, que permite compartir contenido desde el navegador web.
   */

  compartirReto() {
    // Comprobar si la plataforma es Android
    if (this.platform.is('android')) {
      // Usar el plugin de SocialSharing para compartir el mensaje
      this.socialSharing.share(this.mensaje);
    } else {
      // Comprobar si la plataforma soporta la API de navegador share
      if (navigator.share) {
        // Usar la API de navegador share para compartir el mensaje y la url
        navigator
          .share({
            text: this.mensaje,
            url: '',
          })
          // Mostrar un mensaje de éxito en la consola
          .then(() => console.log('Compartido!'))
          // Mostrar un mensaje de error en la consola
          .catch((error) => console.log('Error compartiendo', error));
      }
    }
  }
  /**
   * Método que comprueba si el reto es favorito o no para el usuario logado.
   * Este método se suscribe al servicio `retoSvc`, que devuelve un valor booleano que indica si el reto existe o no en la colección `favoritos` de Firestore.
   * El valor se asigna a la propiedad `esFavorito`, que se usa para mostrar u ocultar el icono de favorito en el ion-card del reto.
   * @param retoId El id del reto
   * @param user El email del usuario
   */
  checkRetoFavorito(retoId: string, user: string) {
    // Suscribirse al servicio `retoSvc`, que devuelve un valor booleano que indica si el reto existe o no en la colección `favoritos` de Firestore
    this.retoSvc.checkFavorito(retoId, user).subscribe((existe) => {
      // Asignar el valor a la propiedad `esFavorito`
      this.esFavorito = existe;
      // Mostrar el valor en la consola
      console.log(this.esFavorito);
    });
  }

  /**
   * Método para marcar como Favorito un Reto concreto al usuario logado.
   * Este método llama al servicio `retoSvc`, que añade el reto a la colección `favoritos` de Firestore.
   * Al finalizar, muestra el toast con el mensaje de éxito o error, usando el servicio `avisosSvc`.
   * @param retoId El id del reto a marcar como Favorito
   * @param user El email del usuario que marca el Favorito
   */
  setFavorito(retoId: string, user: string) {
    // Usar un bloque try-catch para manejar posibles errores
    try {
      // Llamar al servicio `retoSvc`, que añade el reto a la colección `favoritos` de Firestore
      this.retoSvc.addFavorito(retoId, user);
      // Mostrar el toast con el mensaje de éxito, usando el servicio `avisosSvc`
      this.avisosSvc.presentToast('Favorito añadido', 'success');
    } catch (error) {
      // Mostrar el toast con el mensaje de error, usando el servicio `avisosSvc`
      this.avisosSvc.presentToast('Error al añadir Favorito', 'danger');
    }
  }

  /**
   * Método para quitar un Reto de la lista de Favoritos del usuario logado.
   * Este método usa el servicio de RetoService para obtener el id del favorito a partir del id del reto y el email del usuario, y luego eliminar el favorito de la colección de favoritos del usuario en Firestore.
   * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
   * @param {string} retoId El id del reto a quitar de Favoritos
   * @param {string} email El email del usuario que quita el Favorito
   * @returns {void}
   */

  quitarFavorito(retoId: string, email: string) {
    // Usar un bloque try-catch para manejar posibles errores

    try {
      // Obtener el id del favorito a partir del id del reto y el email del usuario usando el servicio de RetoService
      this.retoSvc.getFavorito(retoId, email).subscribe((fav) => {
        // Eliminar el favorito de la colección de favoritos del usuario usando el servicio de RetoService
        this.retoSvc.deleteFavorito(fav[0].ID_FAV);
      });

      // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
      this.avisosSvc.presentToast(
        'Favorito eliminado correctamente',
        'success'
      );
    } catch (error) {
      // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
      this.avisosSvc.presentToast('Error al eliminar el Favorito', 'danger');
    }
  }

  /**
   * Método que comprueba si el reto es conseguido o no por el usuario logado.
   * Este método usa el servicio de RetoService para obtener un observable que indica si el reto existe o no en la colección de retos conseguidos del usuario.
   * El observable se suscribe y asigna el valor a la propiedad esRetoConseguido, que se usa para mostrar el icono de logro en la vista.
   * @param {string} retoId El id del reto a comprobar
   * @param {string} user El email del usuario logado
   * @returns {void}
   */

  checkRetoConseguido(retoId: string, user: string) {
    // Obtener el observable que indica si el reto es conseguido o no por el usuario logado usando el servicio de RetoService
    this.retoSvc.checkRetoConseguido(retoId, user).subscribe((existe) => {
      // Asignar el valor del observable a la propiedad esRetoConseguido
      this.esRetoConseguido = existe;

      // Mostrar el valor en la consola
      console.log(this.esRetoConseguido);
    });
  }

  /**
   * Método para marcar como Conseguido un Reto concreto por el usuario logado.
   * Este método usa el servicio de RetoService para añadir el reto a la colección de retos conseguidos del usuario en Firestore.
   * También usa el servicio de UserService para obtener el total de puntos del usuario y actualizarlo con los puntos del reto.
   * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
   * @param {string} retoId El id del reto a marcar como Conseguido
   * @param {string} user El email del usuario que marca el Conseguido
   * @returns {void}
   */
  setRetoConseguido(retoId: string, user: string) {
    // Usar un bloque try-catch para manejar posibles errores
    try {
      // Mostrar un mensaje en la consola indicando que se va a conseguir el reto
      console.log('vamos a conseguir el reto');

      // Añadir el reto a la colección de retos conseguidos del usuario usando el servicio de RetoService
      this.retoSvc.addRetoConseguido(retoId, user);

      // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
      this.avisosSvc.presentToast('Reto conseguido', 'success');

      // Obtener el total de puntos del usuario usando el servicio de UserService
      this.userSvc.getTotalPuntosByUser(this.email).subscribe((totalPuntos) => {
        // Actualizar el total de puntos del usuario con los puntos del reto usando el servicio de UserService
        this.userSvc.updateUserPuntos(this.email, totalPuntos);
      });
    } catch (error) {
      // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
      this.avisosSvc.presentToast('Error al conseguir Reto', 'danger');
    }
  }

  /**
   * Método para quitar un Reto de la lista de Retos Conseguidos del usuario logado.
   * Este método usa el servicio de RetoService para obtener el id del reto conseguido a partir del id del reto y el email del usuario, y luego eliminar el reto conseguido de la colección de retos conseguidos del usuario en Firestore.
   * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
   * @param {string} retoId El id del reto a quitar de Retos Conseguidos
   * @param {string} email El email del usuario que quita el Conseguido
   * @returns {void}
   */
  removeRetoConseguido(retoId: string, email: string) {
    // Usar un bloque try-catch para manejar posibles errores
    try {
      // Obtener el id del reto conseguido a partir del id del reto y el email del usuario usando el servicio de RetoService
      this.retoSvc
        .getRetoConseguido(retoId, email)
        .subscribe((retoConseguido) => {
          // Eliminar el reto conseguido de la colección de retos conseguidos del usuario usando el servicio de RetoService
          this.retoSvc.deleteRetoConseguido(
            retoConseguido[0].ID_RETO_CONSEGUIDO
          );

          // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
          this.avisosSvc.presentToast('Reto Conseguido eliminado', 'success');
        });
    } catch (error) {
      // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
      this.avisosSvc.presentToast('Error al eliminar Reto', 'danger');
    }
  }

  /**
   * Método que cierra el modal para volver al listado de retos.
   * Este método usa el controlador de Modal de Ionic para cerrar el modal que muestra la información del reto.
   * @param {void}
   * @returns {void}
   */
  cierreModal() {
    // Cerrar el modal usando el controlador de Modal de Ionic
    this.modalCtrl.dismiss();
  }
}
