import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuOpts } from '../../interfaces/interfaces';

/**
 * Componente que muestra un menú lateral con las opciones de navegación disponibles según el rol del usuario.
 * Este componente se muestra como un ion-menu con un ion-list de ion-items, cada uno con un icono, un título y una url de la opción de menú.
 * Al pulsar un ion-item, se usa el servicio de Router para navegar a la url correspondiente de la opción de menú.
 * Este componente se usa en todas las páginas del proyecto, excepto en las de acceso anónimo, donde se oculta el menú.
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  /**
   * Propiedad de entrada que recibe como parámetro de entrada los elementos de menú disponibles según el rol del usuario.
   * Cada elemento de menú es un objeto con las propiedades icon, name y url, que indican el icono, el título y la url de la opción de menú, respectivamente.
   * @type {MenuOpts[]} El array de elementos de menú
   */
  @Input() menuOpts: MenuOpts[];

  /**
   * Constructor de la clase
   * @param router Servicio de Angular que permite navegar entre las páginas de la aplicación mediante las urls
   */
  constructor(private router: Router) {}

  /**
   * Método de inicio que se ejecuta cuando el componente se inicializa.
   * No hace nada en este caso.
   */
  ngOnInit() {}

  /**
   * Método que navega a la página solicitada.
   * Este método usa el servicio de Router para navegar a la url pasada por parámetro, que corresponde a una de las opciones de menú.
   * @param {string} url La url a la que va a navegar
   * @returns {void}
   */
  navigateToPage(url: string) {
    // Navegar a la url usando el servicio de Router
    this.router.navigate([url]);
  }
}
