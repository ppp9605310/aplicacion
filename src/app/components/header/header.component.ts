// Importar los módulos necesarios
import { Component, Input, OnInit } from '@angular/core';

/**
 * Componente que standariza las cabeceras de todas las páginas del proyecto.
 * Este componente se muestra como una barra superior en la parte superior de la pantalla.
 * Contiene el título de la página, que se puede personalizar mediante una propiedad de entrada.
 * También contiene un icono de menú, que permite acceder al menú lateral de navegación.
 * Este componente se usa en todas las páginas del proyecto, excepto en las de acceso anónimo
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  /**
   * Propiedad de entrada que recibe el título de la página como parámetro para personalizar el título.
   * @type {string} El título de la página
   */
  @Input() titulo: string = '';

  /**
   * Constructor de la clase
   * No hace nada en este caso
   */
  constructor() {}

  /**
   * Método de inicio que se ejecuta cuando el componente se inicializa
   * No hace nada en este caso
   */
  ngOnInit() {}
}
