"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_mis-retos_mis-retos_module_ts"],{

/***/ 877:
/*!****************************************************************!*\
  !*** ./src/app/pages/mis-retos/mis-retos.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtaXMtcmV0b3MucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 9560:
/*!****************************************************************!*\
  !*** ./src/app/pages/mis-retos/mis-retos.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<app-header titulo=\"Mis Retos\"></app-header>\r\n<ion-content>\r\n  <ion-item-sliding *ngFor=\"let reto of retos\">\r\n    <ion-item (click)=\"verDetalle(reto.ID)\">\r\n      <ion-avatar slot=\"start\">\r\n        <img [src]=\"reto.IMAGEN\" alt=\"Reto Image\" />\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <h3>{{ reto.TITULO }} ({{reto.ACTIVO}})</h3>\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options side=\"end\">\r\n      <ion-item-option color=\"success\" (click)=\"cambiarEstado(reto.ID)\">\r\n        <ion-icon slot=\"icon-only\" name=\"sync-circle-outline\"></ion-icon>\r\n      </ion-item-option>\r\n      <ion-item-option color=\"danger\">\r\n        <ion-icon\r\n          slot=\"icon-only\"\r\n          name=\"trash\"\r\n          (click)=\"deleteReto(reto.ID)\"\r\n        ></ion-icon>\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"add\" (click)=\"newReto(usuario.EMAIL)\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n\r\n  <app-menu [menuOpts]=\"this.menuSvc.menuOpts\"></app-menu>\r\n</ion-content>\r\n";

/***/ }),

/***/ 1854:
/*!*************************************************************!*\
  !*** ./src/app/pages/mis-retos/mis-retos-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MisRetosPageRoutingModule": () => (/* binding */ MisRetosPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _mis_retos_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mis-retos.page */ 2111);




const routes = [
    {
        path: '',
        component: _mis_retos_page__WEBPACK_IMPORTED_MODULE_0__.MisRetosPage
    }
];
let MisRetosPageRoutingModule = class MisRetosPageRoutingModule {
};
MisRetosPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], MisRetosPageRoutingModule);



/***/ }),

/***/ 1105:
/*!*****************************************************!*\
  !*** ./src/app/pages/mis-retos/mis-retos.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MisRetosPageModule": () => (/* binding */ MisRetosPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _mis_retos_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mis-retos-routing.module */ 1854);
/* harmony import */ var _mis_retos_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mis-retos.page */ 2111);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let MisRetosPageModule = class MisRetosPageModule {
};
MisRetosPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _mis_retos_routing_module__WEBPACK_IMPORTED_MODULE_0__.MisRetosPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_mis_retos_page__WEBPACK_IMPORTED_MODULE_1__.MisRetosPage]
    })
], MisRetosPageModule);



/***/ }),

/***/ 2111:
/*!***************************************************!*\
  !*** ./src/app/pages/mis-retos/mis-retos.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MisRetosPage": () => (/* binding */ MisRetosPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _mis_retos_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mis-retos.page.html?ngResource */ 9560);
/* harmony import */ var _mis_retos_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mis-retos.page.scss?ngResource */ 877);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ 7556);
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ 3071);
/* harmony import */ var _services_menu_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/menu.service */ 8914);
/* harmony import */ var _services_reto_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/reto.service */ 1268);
/* harmony import */ var _services_avisos_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/avisos.service */ 9595);
/* harmony import */ var src_app_components_new_reto_new_reto_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/new-reto/new-reto.component */ 6356);
/* harmony import */ var src_app_components_info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/components/info-reto/info-reto.component */ 8083);












let MisRetosPage = class MisRetosPage {
    /**
     * Constructor de Clase, asigna el menu correspondiente dependiendo del rol
     * @param menuSvc Servicio que prepara el menú
     * @param retoSvc Servicio para el manejo de los Retos
     * @param authSvc Servicio que se encarga de comprobar el usuario logado
     * @param userSvc Servicio que maneja al usuario logado para obtener el rol
     * @param avisosSvc Servicio de avisos a través del Toast
     * @param modalCtrl Controlador de carga del Modal de creación de reto.
     */
    constructor(menuSvc, retoSvc, authSvc, userSvc, avisosSvc, modalCtrl) {
        this.menuSvc = menuSvc;
        this.retoSvc = retoSvc;
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.avisosSvc = avisosSvc;
        this.modalCtrl = modalCtrl;
        /**
         * array de objetos tipo Reto para guardarlos y mostrarlos
         */
        this.retos = [];
        this.menuSvc.setMenu();
    }
    /**
     * Metodo de inicio.
     * Comprueba el usuario logado para el menú y carga sus retos.
     */
    ngOnInit() {
        this.authSvc.getUserEmail().then(email => {
            this.email = email;
            this.userSvc.getUserByEmail(email).subscribe(usuario => {
                this.usuario = usuario;
            });
            this.retoSvc.getRetosByUser(email).subscribe(retos => {
                this.retos = retos;
            });
        });
    }
    /**
     * Carga el Modal con el formulario para crear un nuevo reto
     * @param userId Pasa como parámetro el id del usuario que creará el reto
     */
    newReto(userId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: src_app_components_new_reto_new_reto_component__WEBPACK_IMPORTED_MODULE_7__.NewRetoComponent,
                componentProps: {
                    userId
                },
                cssClass: 'modalInfo'
            });
            modal.present();
        });
    }
    /**
     * Cambia el reto a activo/inactivo y muestra el aviso con el resultado.
     * @param retoId Id del reto a cambiar
     */
    cambiarEstado(retoId) {
        console.log('cambio Estado', retoId);
        try {
            this.retoSvc.updateEstadoReto(retoId);
            this.avisosSvc.presentToast('Cambio de Estado Correcto', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error en el cambio de Estado', 'danger');
        }
    }
    /**
     * Elimina el reto seleccionado
     * @param retoId Id del reto a eliminar
     */
    deleteReto(retoId) {
        try {
            this.retoSvc.deleteReto(retoId);
            this.avisosSvc.presentToast('Reto eliminado correctamente', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar el usuario', 'danger');
        }
    }
    /**
     * Carga el modal con la info detallada del reto
     * @param retoId Id del reto a mostrar
     */
    verDetalle(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: src_app_components_info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_8__.InfoRetoComponent,
                componentProps: {
                    retoId
                },
                cssClass: 'modalInfo'
            });
            modal.present();
        });
    }
};
MisRetosPage.ctorParameters = () => [
    { type: _services_menu_service__WEBPACK_IMPORTED_MODULE_4__.MenuService },
    { type: _services_reto_service__WEBPACK_IMPORTED_MODULE_5__.RetoService },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__.UserService },
    { type: _services_avisos_service__WEBPACK_IMPORTED_MODULE_6__.AvisosService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.ModalController }
];
MisRetosPage = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-mis-retos',
        template: _mis_retos_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_mis_retos_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], MisRetosPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_mis-retos_mis-retos_module_ts.js.map