"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_about_about_module_ts"],{

/***/ 4229:
/*!********************************************************!*\
  !*** ./src/app/pages/about/about.page.scss?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "@charset \"UTF-8\";\nion-icon {\n  font-size: 72px;\n  text-align: center;\n}\nion-img {\n  height: 150px;\n  width: 150px;\n  margin: 0 auto;\n}\n.color-twitter {\n  color: #00acee;\n}\n.color-linkedin {\n  color: #0e76a8;\n}\nspan,\nh3 {\n  color: var(--ion-color-primary);\n}\nh3 {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  text-decoration: underline;\n}\n.custom-button {\n  max-width: 180px;\n  margin: 0 auto;\n  width: 100%;\n}\n.small-icon {\n  font-size: 18px;\n}\n.bold-text {\n  font-size: 18px;\n  font-weight: bold;\n}\n.logo-image {\n  width: 200px;\n  height: auto;\n  margin: 0 auto;\n  max-width: 250px;\n}\n/* el botón responsive */\n@media (max-width: 576px) {\n  .custom-button {\n    max-width: 60%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFib3V0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEI7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUFFRjtBQUNBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FBRUY7QUFDQTtFQUNFLGNBQUE7QUFFRjtBQUNBO0VBQ0UsY0FBQTtBQUVGO0FBQ0E7O0VBRUUsK0JBQUE7QUFFRjtBQUNBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0FBRUY7QUFDQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUFFRjtBQUNBO0VBQ0UsZUFBQTtBQUVGO0FBQ0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFFRjtBQUNBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFFRjtBQUNBLHdCQUFBO0FBQ0E7RUFDRTtJQUNFLGNBQUE7RUFFRjtBQUNGIiwiZmlsZSI6ImFib3V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pY29uIHtcclxuICBmb250LXNpemU6IDcycHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5pb24taW1nIHtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG5cclxuLmNvbG9yLXR3aXR0ZXIge1xyXG4gIGNvbG9yOiAjMDBhY2VlO1xyXG59XHJcblxyXG4uY29sb3ItbGlua2VkaW4ge1xyXG4gIGNvbG9yOiAjMGU3NmE4O1xyXG59XHJcblxyXG5zcGFuLFxyXG5oMyB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuaDMge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxufVxyXG5cclxuLmN1c3RvbS1idXR0b24ge1xyXG4gIG1heC13aWR0aDogMTgwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zbWFsbC1pY29uIHtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5ib2xkLXRleHQge1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmxvZ28taW1hZ2Uge1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgbWF4LXdpZHRoOiAyNTBweDtcclxufVxyXG5cclxuLyogZWwgYm90w7NuIHJlc3BvbnNpdmUgKi9cclxuQG1lZGlhIChtYXgtd2lkdGg6IDU3NnB4KSB7XHJcbiAgLmN1c3RvbS1idXR0b24ge1xyXG4gICAgbWF4LXdpZHRoOiA2MCU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */";

/***/ }),

/***/ 5874:
/*!********************************************************!*\
  !*** ./src/app/pages/about/about.page.html?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "<app-header titulo=\"Mi Información\"></app-header>\r\n\r\n<ion-content>\r\n  <ion-grid fixed class=\"ion-height-full ion-align-items-center\">\r\n    <ion-row class=\"ion-justify-content-center\">\r\n      <ion-col size-lg=\"6\" size-md=\"8\" size-xs=\"12\">\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <ion-img\r\n              src=\"./assets/images/logoAstroRetos.png\"\r\n              class=\"logo-image\"\r\n            ></ion-img>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-card class=\"ion-text-center\">\r\n                <ion-card-content>\r\n                  <h3>Aplicación para Proyecto Final</h3>\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-card class=\"ion-text-center\">\r\n                <ion-card-content>\r\n                  <p>Curso:</p>\r\n                  <a\r\n                    href=\"https://www3.gobiernodecanarias.org/medusa/edublog/ieselrincon/\"\r\n                    target=\"_blank\"\r\n                    rel=\"noopener noreferrer\"\r\n                    >3DAMN IES EL RINCON</a\r\n                  >\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <!-- Repite este patrón para las filas restantes -->\r\n\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-card class=\"ion-text-center\">\r\n                <ion-card-content>\r\n                  <p>Alumno:</p>\r\n                  <a\r\n                    href=\"https://github.com/EstudiaGit\"\r\n                    target=\"_blank\"\r\n                    rel=\"noopener noreferrer\"\r\n                    >Amós Abelleira Pérez</a\r\n                  >\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <!-- ... otros ion-row ... -->\r\n\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-card class=\"ion-text-center\">\r\n                <ion-card-content>\r\n                  <p>Enlace Documentación:</p>\r\n                  <a\r\n                    href=\"https://estudiagit.github.io/documentacion_PPP/index.html\"\r\n                    target=\"_blank\"\r\n                    rel=\"noopener noreferrer\"\r\n                    >Documentación</a\r\n                  >\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-card class=\"ion-text-center\">\r\n                <ion-card-content>\r\n                  <p>Enlace proyecto GitHub:</p>\r\n                  <a\r\n                    href=\"https://gitlab.com/ppp9605310/aplicacion\"\r\n                    target=\"_blank\"\r\n                    rel=\"noopener noreferrer\"\r\n                    >Código</a\r\n                  >\r\n                </ion-card-content>\r\n              </ion-card>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <ion-footer class=\"ion-padding-top\">\r\n          <ion-row class=\"ion-text-center\">\r\n            <ion-col size=\"12\">\r\n              <ion-button\r\n                expand=\"block\"\r\n                fill=\"outline\"\r\n                color=\"primary\"\r\n                routerLink=\"/home\"\r\n                routerDirection=\"root\"\r\n                class=\"custom-button\"\r\n              >\r\n                <ion-icon\r\n                  slot=\"start\"\r\n                  name=\"home\"\r\n                  class=\"small-icon\"\r\n                ></ion-icon>\r\n                <ion-label class=\"bold-text\">Inicio</ion-label>\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-footer>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n";

/***/ }),

/***/ 3423:
/*!*****************************************************!*\
  !*** ./src/app/pages/about/about-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AboutPageRoutingModule": () => (/* binding */ AboutPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./about.page */ 388);




const routes = [
    {
        path: '',
        component: _about_page__WEBPACK_IMPORTED_MODULE_0__.AboutPage
    }
];
let AboutPageRoutingModule = class AboutPageRoutingModule {
};
AboutPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AboutPageRoutingModule);



/***/ }),

/***/ 8114:
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AboutPageModule": () => (/* binding */ AboutPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _about_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./about-routing.module */ 3423);
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./about.page */ 388);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let AboutPageModule = class AboutPageModule {
};
AboutPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _about_routing_module__WEBPACK_IMPORTED_MODULE_0__.AboutPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_about_page__WEBPACK_IMPORTED_MODULE_1__.AboutPage]
    })
], AboutPageModule);



/***/ }),

/***/ 388:
/*!*******************************************!*\
  !*** ./src/app/pages/about/about.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AboutPage": () => (/* binding */ AboutPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _about_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./about.page.html?ngResource */ 5874);
/* harmony import */ var _about_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./about.page.scss?ngResource */ 4229);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 7122);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 3819);






let AboutPage = class AboutPage {
    /**
     * Constructor de clase
     * @param iab Componente para mostrar los enlaces a perfiles en el navegador integrado a traves de InAppBrowser
     * @param platform Componente para controlar la plataforma donde se ejecuta la app
     */
    constructor(iab, platform) {
        this.iab = iab;
        this.platform = platform;
    }
    /**
     * Metodo de inicio
     */
    ngOnInit() {
    }
    /**
     * Abre los perfiles
     * @param url URL que se tiene que abrir
     * @returns
     */
    openURL(url) {
        if (this.platform.is('android')) {
            const browser = this.iab.create(url);
            browser.show();
            return;
        }
    }
};
AboutPage.ctorParameters = () => [
    { type: _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__.InAppBrowser },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform }
];
AboutPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-about',
        template: _about_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_about_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AboutPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_about_about_module_ts.js.map