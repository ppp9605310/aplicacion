"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_news-page_news-page_module_ts"],{

/***/ 5043:
/*!****************************************************************!*\
  !*** ./src/app/pages/news-page/news-page.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJuZXdzLXBhZ2UucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 6492:
/*!****************************************************************!*\
  !*** ./src/app/pages/news-page/news-page.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<app-header titulo=\"Noticias\"></app-header>\r\n\r\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onScroll($event)\">\r\n\r\n  <app-noticias [articles]=\"articles\"></app-noticias>\r\n\r\n  <ion-infinite-scroll (ionInfinite)=\"loadMore($event)\">\r\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\"\r\n      loadingText=\"Cargando más noticias...\"></ion-infinite-scroll-content>\r\n  </ion-infinite-scroll>\r\n\r\n  <app-menu [menuOpts]=\"this.menuSvc.menuOpts\"></app-menu>\r\n\r\n</ion-content>\r\n\r\n";

/***/ }),

/***/ 30:
/*!*************************************************************!*\
  !*** ./src/app/pages/news-page/news-page-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewsPagePageRoutingModule": () => (/* binding */ NewsPagePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _news_page_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./news-page.page */ 1224);




const routes = [
    {
        path: '',
        component: _news_page_page__WEBPACK_IMPORTED_MODULE_0__.NewsPagePage
    }
];
let NewsPagePageRoutingModule = class NewsPagePageRoutingModule {
};
NewsPagePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], NewsPagePageRoutingModule);



/***/ }),

/***/ 5970:
/*!*****************************************************!*\
  !*** ./src/app/pages/news-page/news-page.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewsPagePageModule": () => (/* binding */ NewsPagePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _news_page_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./news-page-routing.module */ 30);
/* harmony import */ var _news_page_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./news-page.page */ 1224);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let NewsPagePageModule = class NewsPagePageModule {
};
NewsPagePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _news_page_routing_module__WEBPACK_IMPORTED_MODULE_0__.NewsPagePageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_news_page_page__WEBPACK_IMPORTED_MODULE_1__.NewsPagePage]
    })
], NewsPagePageModule);



/***/ }),

/***/ 1224:
/*!***************************************************!*\
  !*** ./src/app/pages/news-page/news-page.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewsPagePage": () => (/* binding */ NewsPagePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _news_page_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./news-page.page.html?ngResource */ 6492);
/* harmony import */ var _news_page_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./news-page.page.scss?ngResource */ 5043);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _services_news_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/news.service */ 1857);
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/menu.service */ 8914);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user.service */ 3071);








let NewsPagePage = class NewsPagePage {
    /**
     * Contructor de clase
     * @param menuSvc Servicio que controla los elementos a mostrar en el menú
     * @param authSvc Servicio para comprobar el usuario logado
     * @param userSvc Servicio para manejar los datos del usuario
     * @param newsSvc Servicio de carga de noticias
     */
    constructor(menuSvc, authSvc, userSvc, newsSvc) {
        this.menuSvc = menuSvc;
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.newsSvc = newsSvc;
        /**
         * Variable pública para manejo de los artículos
         */
        this.articles = [];
        /**
         * Variable para guardar la pagina cargada
         */
        this.page = 1;
        /**
         * Numero de noticias por pagina
         */
        this.pageSize = 10;
        this.menuSvc.setMenu();
    }
    /**
     * Metodo de inicio
     * Comprueba el usuario logado para posteriormente cargar su menu
     * Carga las noticias
     */
    ngOnInit() {
        this.authSvc.getUserEmail().then(email => {
            this.email = email;
            this.userSvc.getUserByEmail(email).subscribe(usuario => {
                this.usuario = usuario;
            });
        });
        this.loadNews();
    }
    /**
     * Carga las cabeceras de las noticias
     */
    loadNews() {
        this.newsSvc.getTopHeadLines()
            .subscribe(articles => this.articles.push(...articles));
    }
    /**
     * Carga más noticias pasando de pagina
     * @param event Evento que desencadena la carga de mas noticias: llegar casi al final de las ya cargadas
     */
    loadMore(event) {
        this.page++;
        this.loadNews();
        event.target.complete();
    }
    /**
     * Usado para desencadenar la carga de noticias
     * @param event Evento desencadenador
     */
    onScroll(event) {
        const scrollElement = event.target;
        const scrollHeight = scrollElement.scrollHeight;
    }
};
NewsPagePage.ctorParameters = () => [
    { type: src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_3__.MenuService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__.AuthService },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: _services_news_service__WEBPACK_IMPORTED_MODULE_2__.NewsService }
];
NewsPagePage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-news-page',
        template: _news_page_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_news_page_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], NewsPagePage);



/***/ }),

/***/ 1857:
/*!******************************************!*\
  !*** ./src/app/services/news.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewsService": () => (/* binding */ NewsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 6942);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/environments/environment */ 2340);





/**
 * API del servicio de noticias apinews
 */
const apiKey = src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.news.apiKey;
/**
 * URL del servicio
 */
const apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.news.apiUrl;
let NewsService = class NewsService {
    /**
     * Constructor de clase
     * @param http Servicio http para solicitar la llamada a la api
     */
    constructor(http) {
        this.http = http;
    }
    /**
     * Ejecuta la consulta a la API con los filtros pasados por el endpoint
     * @param endpoint cadena pasada con las variables que necesita la solicitud a la api
     * @returns
     */
    executeQuery(endpoint) {
        return this.http.get(`${apiUrl}${endpoint}`, {
            params: {
                apiKey,
                country: 'us',
            },
        });
    }
    /**
     * Devuelve un observable con las cabeceras de las noticias a mostrar.
     * Aqui pasa por parametro la categoria ciencia y espacio
     * @returns
     */
    getTopHeadLines() {
        return this.executeQuery(`/top-headlines?category=science&q=space`).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(({ articles }) => articles));
    }
};
NewsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient }
];
NewsService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root',
    })
], NewsService);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_news-page_news-page_module_ts.js.map