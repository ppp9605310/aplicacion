"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_admin_admin_module_ts"],{

/***/ 1586:
/*!********************************************************!*\
  !*** ./src/app/pages/admin/admin.page.scss?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZG1pbi5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 5959:
/*!********************************************************!*\
  !*** ./src/app/pages/admin/admin.page.html?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-segment value=\"retos\" (ionChange)=\"segmentChanged($event)\">\r\n      <ion-segment-button value=\"retos\">\r\n        <ion-label>Retos</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"users\">\r\n        <ion-label>Usuarios</ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <div *ngIf=\"segmento == 'retos'\">\r\n    <ion-list>\r\n\r\n      <ion-item-sliding *ngFor=\"let reto of retos\">\r\n        <ion-item>\r\n          <ion-avatar slot=\"start\">\r\n            <img [src]=\"reto.IMAGEN\" />\r\n          </ion-avatar>\r\n          <ion-label>\r\n            <h3>{{ reto.TITULO }} ({{reto.ACTIVO}})</h3>\r\n           \r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-item-options side=\"end\">\r\n          <ion-item-option color=\"success\" (click)=\"cambiarEstado(reto.ID)\">\r\n           <ion-icon slot=\"icon-only\" name=\"sync-circle-outline\"></ion-icon>\r\n          </ion-item-option>\r\n          <ion-item-option color=\"danger\">\r\n            <ion-icon slot=\"icon-only\" name=\"trash\" (click)=\"deleteReto(reto.ID)\"></ion-icon>\r\n          </ion-item-option>\r\n        </ion-item-options>\r\n      </ion-item-sliding>\r\n\r\n    </ion-list>\r\n  </div>\r\n  <div *ngIf=\"segmento == 'users'\">\r\n    <ion-list>\r\n\r\n      <ion-item-sliding *ngFor=\"let usuario of usuarios\">\r\n        <ion-item>\r\n          <ion-avatar slot=\"start\">\r\n            <img [src]=\"usuario.AVATAR\" />\r\n          </ion-avatar>\r\n          <ion-label>\r\n            <h3>{{ usuario.NOMBRE }}  ({{usuario.ROL}}) </h3>\r\n            <p><small>{{ usuario.EMAIL }}</small></p>\r\n          </ion-label>\r\n          <ion-badge slot=\"end\">{{ usuario.PUNTOS }}</ion-badge>\r\n        </ion-item>\r\n        <ion-item-options side=\"end\">\r\n          <ion-item-option (click)=\"cambiarRol(usuario.ID)\" color=\"success\">\r\n            <ion-icon slot=\"icon-only\" name=\"sync-circle-outline\"></ion-icon>\r\n          </ion-item-option>\r\n          <ion-item-option (click)=\"deleteUser(usuario.ID)\" color=\"danger\">\r\n            <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\r\n          </ion-item-option>\r\n        </ion-item-options>\r\n      </ion-item-sliding>\r\n\r\n    </ion-list>\r\n  </div>\r\n\r\n  <app-menu [menuOpts]=\"this.menuSvc.menuOpts\"></app-menu>\r\n</ion-content>";

/***/ }),

/***/ 6500:
/*!*****************************************************!*\
  !*** ./src/app/pages/admin/admin-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPageRoutingModule": () => (/* binding */ AdminPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _admin_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin.page */ 2222);




const routes = [
    {
        path: '',
        component: _admin_page__WEBPACK_IMPORTED_MODULE_0__.AdminPage
    }
];
let AdminPageRoutingModule = class AdminPageRoutingModule {
};
AdminPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AdminPageRoutingModule);



/***/ }),

/***/ 1496:
/*!*********************************************!*\
  !*** ./src/app/pages/admin/admin.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPageModule": () => (/* binding */ AdminPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin-routing.module */ 6500);
/* harmony import */ var _admin_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin.page */ 2222);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let AdminPageModule = class AdminPageModule {
};
AdminPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _admin_routing_module__WEBPACK_IMPORTED_MODULE_0__.AdminPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_admin_page__WEBPACK_IMPORTED_MODULE_1__.AdminPage]
    })
], AdminPageModule);



/***/ }),

/***/ 2222:
/*!*******************************************!*\
  !*** ./src/app/pages/admin/admin.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPage": () => (/* binding */ AdminPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _admin_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin.page.html?ngResource */ 5959);
/* harmony import */ var _admin_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin.page.scss?ngResource */ 1586);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user.service */ 3071);
/* harmony import */ var _services_avisos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/avisos.service */ 9595);
/* harmony import */ var _services_reto_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/reto.service */ 1268);
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/menu.service */ 8914);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);









let AdminPage = class AdminPage {
    /**
     * Constructor de clase
     * @param userSvc Servicio para el manejo de datos de usuario
     * @param menuSvc Servicio para la personalización del Menu
     * @param authSvc Servicio para la gestion de la Autenticacion
     * @param avisosSvc Servicio para controlar avisos con el Toast
     * @param retoSvc Servicio para el manejo de los Retos
     */
    constructor(userSvc, menuSvc, authSvc, avisosSvc, retoSvc) {
        this.userSvc = userSvc;
        this.menuSvc = menuSvc;
        this.authSvc = authSvc;
        this.avisosSvc = avisosSvc;
        this.retoSvc = retoSvc;
        /**
         * Array de objetos de tipo Usuario
         */
        this.usuarios = [];
        /**
         * Array de objetos de tipo Reto
         */
        this.retos = [];
        this.menuSvc.setMenu();
    }
    /**
     * Metodo de inicio
     * Comprueba el usuario logado para almacenar los datos de su perfil
     * Obtiene tambien la lista de todos los retos y usuarios para mostrarlos
     */
    ngOnInit() {
        this.authSvc.getUserEmail().then(email => {
            this.userEmail = email;
            this.userSvc.getUserByEmail(this.userEmail).subscribe(usuario => {
                this.usuario = usuario;
            });
        });
        this.retoSvc.getRetos().subscribe(retos => {
            this.retos = retos;
        });
        this.userSvc.getUsers().subscribe(users => {
            this.usuarios = users;
        });
    }
    /**
     * Metodo que realiza el cambio de segmento al recibir un evento
     * @param event Evento (cambio de segmento)
     */
    segmentChanged(event) {
        this.segmento = event.detail.value;
        this.retoSvc.getRetos().subscribe(retos => {
            this.retos = retos;
        });
    }
    /**
     * Método para cambiar el Rol de un usuario concreto Retador<=>Administrador
     * @param userId Id del usuario
     */
    cambiarRol(userId) {
        console.log('cambio rol', userId);
        try {
            this.userSvc.updateUserRol(userId);
            this.avisosSvc.presentToast('Cambio de Rol Correcto', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error en el cambio de Rol', 'danger');
        }
    }
    /**
     * Metodo que cambia el estado del reto: activo<=>inactivo
     * @param idReto Id del Reto
     */
    cambiarEstado(idReto) {
        console.log('cambio Estado', idReto);
        try {
            this.retoSvc.updateEstadoReto(idReto);
            this.avisosSvc.presentToast('Cambio de Estado Correcto', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error en el cambio de Estado', 'danger');
        }
    }
    /**
     * Metodo que elimina un usuaio
     * @param userId Ide del usuario a borrar
     */
    deleteUser(userId) {
        try {
            this.authSvc.deleteUser(userId);
            this.avisosSvc.presentToast('Usuario eliminado correctamente', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar el usuario', 'danger');
        }
    }
    /**
     * Metodo que elimina un reto
     * @param idReto Id del Reto a eliminar
     */
    deleteReto(idReto) {
        try {
            this.retoSvc.deleteReto(idReto);
            this.avisosSvc.presentToast('Usuario eliminado correctamente', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar el usuario', 'danger');
        }
    }
};
AdminPage.ctorParameters = () => [
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__.UserService },
    { type: src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__.MenuService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__.AuthService },
    { type: _services_avisos_service__WEBPACK_IMPORTED_MODULE_3__.AvisosService },
    { type: _services_reto_service__WEBPACK_IMPORTED_MODULE_4__.RetoService }
];
AdminPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-admin',
        template: _admin_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_admin_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AdminPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_admin_admin_module_ts.js.map