"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_favoritos_favoritos_module_ts"],{

/***/ 5579:
/*!****************************************************************!*\
  !*** ./src/app/pages/favoritos/favoritos.page.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmYXZvcml0b3MucGFnZS5zY3NzIn0= */";

/***/ }),

/***/ 4275:
/*!****************************************************************!*\
  !*** ./src/app/pages/favoritos/favoritos.page.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<app-header titulo=\"Mis Favoritos\"></app-header>\r\n<ion-content>\r\n  <app-menu></app-menu>\r\n\r\n  <ion-item-sliding *ngFor=\"let reto of retos; let i = index\">\r\n    <ion-item (click)=\"verDetalle(reto.ID)\">\r\n      <ion-avatar slot=\"start\">\r\n        <img [src]=\"reto?.IMAGEN\" />\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <h3>{{ reto?.TITULO }}</h3>\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-item-options side=\"end\">\r\n      <ion-item-option color=\"danger\">\r\n        <ion-icon slot=\"icon-only\" name=\"star-outline\" (click)=\"quitarFavorito(this.favoritos[i].ID_FAV)\"></ion-icon>\r\n      </ion-item-option>\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n\r\n\r\n  <app-menu [menuOpts]=\"this.menuSvc.menuOpts\"></app-menu>\r\n</ion-content>";

/***/ }),

/***/ 2602:
/*!*************************************************************!*\
  !*** ./src/app/pages/favoritos/favoritos-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FavoritosPageRoutingModule": () => (/* binding */ FavoritosPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _favoritos_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./favoritos.page */ 5717);




const routes = [
    {
        path: '',
        component: _favoritos_page__WEBPACK_IMPORTED_MODULE_0__.FavoritosPage
    }
];
let FavoritosPageRoutingModule = class FavoritosPageRoutingModule {
};
FavoritosPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FavoritosPageRoutingModule);



/***/ }),

/***/ 8919:
/*!*****************************************************!*\
  !*** ./src/app/pages/favoritos/favoritos.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FavoritosPageModule": () => (/* binding */ FavoritosPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _favoritos_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./favoritos-routing.module */ 2602);
/* harmony import */ var _favoritos_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./favoritos.page */ 5717);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let FavoritosPageModule = class FavoritosPageModule {
};
FavoritosPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _favoritos_routing_module__WEBPACK_IMPORTED_MODULE_0__.FavoritosPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_favoritos_page__WEBPACK_IMPORTED_MODULE_1__.FavoritosPage]
    })
], FavoritosPageModule);



/***/ }),

/***/ 5717:
/*!***************************************************!*\
  !*** ./src/app/pages/favoritos/favoritos.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FavoritosPage": () => (/* binding */ FavoritosPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _favoritos_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./favoritos.page.html?ngResource */ 4275);
/* harmony import */ var _favoritos_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./favoritos.page.scss?ngResource */ 5579);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var src_app_components_info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/info-reto/info-reto.component */ 8083);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/avisos.service */ 9595);
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/menu.service */ 8914);
/* harmony import */ var src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/reto.service */ 1268);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/user.service */ 3071);











let FavoritosPage = class FavoritosPage {
    /**
     * Constructor de clase
     * Aqui cargamos el menu personalizado
     * @param menuSvc Servicio para personalizar el menu
     * @param retoSvc Servicio para el manejo de los retos
     * @param authSvc Servicio para gestionar la Autenticacion
     * @param userSvc Servicio para operaciones con el usuario
     * @param avisosSvc Servicio de avisos a traves de toast
     * @param modalCtrl Servicio que controla la carga de modales
     */
    constructor(menuSvc, retoSvc, authSvc, userSvc, avisosSvc, modalCtrl) {
        this.menuSvc = menuSvc;
        this.retoSvc = retoSvc;
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.avisosSvc = avisosSvc;
        this.modalCtrl = modalCtrl;
        /**
         * Array de objetos de tipo Favorito
         */
        this.favoritos = [];
        /**
         * Array de objetos de tipo Reto
         */
        this.retos = [];
        this.menuSvc.setMenu();
    }
    /**
     * Metodo de inicio
     * Comprobamos el mail del usuario logado
     * Cargamos los favoritos de ese usuario
     * Almacenamos los retos por los ID de los favoritos
     */
    ngOnInit() {
        this.favoritos = [];
        this.authSvc.getUserEmail().then(email => {
            this.userEmail = email;
            this.userSvc.getUserByEmail(email).subscribe(usuario => {
                this.usuario = usuario;
                this.idUser = this.usuario.ID;
                console.log(this.usuario.NOMBRE);
            });
            this.retoSvc.getFavoritosByUser(this.userEmail).subscribe(favoritos => {
                this.favoritos = favoritos;
                console.log(this.favoritos.length);
                for (let index = 0; index < this.favoritos.length; index++) {
                    this.retoSvc.getRetosById(this.favoritos[index].ID_RETO).subscribe(reto => {
                        this.retos = [...this.retos, ...reto];
                        console.log('reto', index, this.retos[index], this.favoritos[index]);
                    });
                }
            });
        });
    }
    /**
     * Muestra el Modal con los detalles del reto
     * @param retoId Id del reto
     */
    verDetalle(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: src_app_components_info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_2__.InfoRetoComponent,
                componentProps: {
                    retoId
                },
                cssClass: 'modalInfo'
            });
            modal.present();
        });
    }
    /**
     * Quita el favorito de la lista
     * @param idFav Id del favorito
     */
    quitarFavorito(idFav) {
        try {
            this.retoSvc.deleteFavorito(idFav);
            console.log('eliminando reto', this.idFavorito, idFav);
            this.avisosSvc.presentToast('Favorito eliminado correctamente', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar el Favorito', 'danger');
        }
    }
};
FavoritosPage.ctorParameters = () => [
    { type: src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_5__.MenuService },
    { type: src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_6__.RetoService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthService },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_7__.UserService },
    { type: src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_4__.AvisosService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ModalController }
];
FavoritosPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-favoritos',
        template: _favoritos_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_favoritos_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FavoritosPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_favoritos_favoritos_module_ts.js.map