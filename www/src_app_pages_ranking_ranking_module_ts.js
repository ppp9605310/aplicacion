"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_ranking_ranking_module_ts"],{

/***/ 485:
/*!************************************************************!*\
  !*** ./src/app/pages/ranking/ranking.page.scss?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyYW5raW5nLnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 9919:
/*!************************************************************!*\
  !*** ./src/app/pages/ranking/ranking.page.html?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "<app-header titulo = \"Top Score\"></app-header>\r\n\r\n<ion-content>\r\n\r\n<ion-list>\r\n    <ion-item *ngFor=\"let usuario of usuarios\">\r\n        <ion-label>{{usuario.NOMBRE}}</ion-label>\r\n        <ion-label>{{usuario.PUNTOS}}</ion-label>\r\n    </ion-item>\r\n</ion-list>\r\n\r\n<app-menu [menuOpts]=\"this.menuSvc.menuOpts\"></app-menu>\r\n</ion-content>\r\n";

/***/ }),

/***/ 4892:
/*!*********************************************************!*\
  !*** ./src/app/pages/ranking/ranking-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RankingPageRoutingModule": () => (/* binding */ RankingPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _ranking_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ranking.page */ 3215);




const routes = [
    {
        path: '',
        component: _ranking_page__WEBPACK_IMPORTED_MODULE_0__.RankingPage
    }
];
let RankingPageRoutingModule = class RankingPageRoutingModule {
};
RankingPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], RankingPageRoutingModule);



/***/ }),

/***/ 4617:
/*!*************************************************!*\
  !*** ./src/app/pages/ranking/ranking.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RankingPageModule": () => (/* binding */ RankingPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _ranking_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ranking-routing.module */ 4892);
/* harmony import */ var _ranking_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ranking.page */ 3215);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let RankingPageModule = class RankingPageModule {
};
RankingPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _ranking_routing_module__WEBPACK_IMPORTED_MODULE_0__.RankingPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_ranking_page__WEBPACK_IMPORTED_MODULE_1__.RankingPage]
    })
], RankingPageModule);



/***/ }),

/***/ 3215:
/*!***********************************************!*\
  !*** ./src/app/pages/ranking/ranking.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RankingPage": () => (/* binding */ RankingPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _ranking_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ranking.page.html?ngResource */ 9919);
/* harmony import */ var _ranking_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ranking.page.scss?ngResource */ 485);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/menu.service */ 8914);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user.service */ 3071);







let RankingPage = class RankingPage {
    /**
     * constructor de clase
     * @param userSvc Servicio personalizado para manejo de Usuarios
     * @param menuSvc Servicio para controlar el menu
     * @param authSvc Servicio para gestionar la Autenticación
     */
    constructor(userSvc, menuSvc, authSvc) {
        this.userSvc = userSvc;
        this.menuSvc = menuSvc;
        this.authSvc = authSvc;
        /**
         * Array de objetos de Usuario
         */
        this.usuarios = [];
        //Cargamos opciones de Menu
        this.menuSvc.setMenu();
    }
    /**Metodos de carga al inicio
     * Comprobamos si está logado y guardamos la info del usuario logado
     * Carga los puntos de cada usuario
     */
    ngOnInit() {
        this.authSvc.getUserEmail().then(email => {
            this.userEmail = email;
            this.userSvc.getUserByEmail(this.userEmail).subscribe(usuario => {
                this.usuario = usuario;
            });
        });
        this.userSvc.getUsuariosPorPuntos().subscribe(usuarios => {
            this.usuarios = usuarios;
        });
    }
};
RankingPage.ctorParameters = () => [
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__.UserService },
    { type: src_app_services_menu_service__WEBPACK_IMPORTED_MODULE_3__.MenuService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService }
];
RankingPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-ranking',
        template: _ranking_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_ranking_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RankingPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_ranking_ranking_module_ts.js.map