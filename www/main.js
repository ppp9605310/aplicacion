(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["main"],{

/***/ 9259:
/*!***********************************************!*\
  !*** ./src/app/app.component.scss?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 3383:
/*!***********************************************!*\
  !*** ./src/app/app.component.html?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-app>\r\n  <ion-router-outlet></ion-router-outlet>\r\n</ion-app>\r\n";

/***/ }),

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services/auth.service */ 7556);




const routes = [
    {
        path: 'home',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_pages_home_home_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/home/home.module */ 7994)).then(m => m.HomePageModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_pages_home_home_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/home/home.module */ 7994)).then(m => m.HomePageModule)
    },
    {
        path: 'perfil',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_pages_perfil_perfil_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/perfil/perfil.module */ 6217)).then(m => m.PerfilPageModule), canActivate: [_services_auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthService]
    },
    {
        path: 'about',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("src_app_pages_about_about_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/about/about.module */ 8114)).then(m => m.AboutPageModule)
    },
    {
        path: 'admin',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_admin_admin_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/admin/admin.module */ 1496)).then(m => m.AdminPageModule), canActivate: [_services_auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthService]
    },
    {
        path: 'ranking',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_ranking_ranking_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/ranking/ranking.module */ 4617)).then(m => m.RankingPageModule)
    },
    {
        path: 'mis-retos',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_mis-retos_mis-retos_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/mis-retos/mis-retos.module */ 1105)).then(m => m.MisRetosPageModule), canActivate: [_services_auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthService]
    },
    {
        path: 'favoritos',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_favoritos_favoritos_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/favoritos/favoritos.module */ 8919)).then(m => m.FavoritosPageModule), canActivate: [_services_auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthService]
    },
    {
        path: 'news-page',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_components_components_module_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_news-page_news-page_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/news-page/news-page.module */ 5970)).then(m => m.NewsPagePageModule)
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_3__.PreloadAllModules })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component.html?ngResource */ 3383);
/* harmony import */ var _app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss?ngResource */ 9259);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);




let AppComponent = class AppComponent {
    /**
   * Constructor del componente
   *
   * SE RECOMIENDA NO TOCAR
   */
    constructor() {
    }
};
AppComponent.ctorParameters = () => [];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-root',
        template: _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AppComponent);



/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var _angular_fire_compat__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/compat */ 1879);
/* harmony import */ var _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/fire/compat/auth */ 5873);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../environments/environment */ 2340);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/compat/storage */ 5574);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 6436);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 7122);








//Imports de Firestore






let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule,
            _angular_fire_compat__WEBPACK_IMPORTED_MODULE_9__.AngularFireModule.initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_2__.environment.firebase),
            _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_10__.AngularFireStorageModule,
            _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_11__.AngularFireAuthModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_12__.HttpClientModule,
        ],
        providers: [{ provide: _angular_router__WEBPACK_IMPORTED_MODULE_13__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicRouteStrategy },
            _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__.SocialSharing,
            _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__.InAppBrowser
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 7556:
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthService": () => (/* binding */ AuthService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_fire_compat_firestore___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/compat/firestore/ */ 2393);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/compat/auth */ 5873);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 3910);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 6942);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 8759);
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.service */ 3071);
/* harmony import */ var _avisos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./avisos.service */ 9595);








let AuthService = class AuthService {
    /**
     * Constructor de clase
     * @param angularFireAuth Servicio de Angular Firebase con los metodos típicos de autenticacion
     * @param userSvc Servicio personalizado para operaciones con el usuario
     * @param firestore Servicio de firestore
     * @param router Servicio de enrutamiento dentro de la aplicación
     * @param avisosSvc Servicio de avisos a través de Toasts
     */
    constructor(angularFireAuth, userSvc, firestore, router, avisosSvc) {
        this.angularFireAuth = angularFireAuth;
        this.userSvc = userSvc;
        this.firestore = firestore;
        this.router = router;
        this.avisosSvc = avisosSvc;
    }
    /**
     * Método de creación de usuarios. Primero crea el usuario en Firestore y también lo crea en Auth de Firebase
     * @param usuario Datos recogidos por el Formulario
     * @returns
     */
    signUp(usuario) {
        const noAvatar = 'https://firebasestorage.googleapis.com/v0/b/astroretos-db.appspot.com/o/avatar%2Fno-avatar.png?alt=media&token=e6384116-00b8-4a34-a0f1-ee14e4ae8983';
        this.firestore
            .collection('usuarios')
            .add({
            EMAIL: usuario.EMAIL,
            NOMBRE: usuario.NOMBRE,
            ROL: 'Retador',
            AVATAR: noAvatar,
            ID: '',
            PUNTOS: 0,
        })
            .then((docRef) => {
            this.firestore.doc(docRef).update({
                ID: docRef.id,
            });
        })
            .catch((error) => {
            this.avisosSvc.presentToast('No se ha podido crear el Usuario', 'danger');
        });
        return new Promise((resolve, reject) => {
            this.angularFireAuth
                .createUserWithEmailAndPassword(usuario.EMAIL, usuario.PASSWORD)
                .then((res) => resolve(res), (err) => reject(err));
        });
    }
    /**
     * Método para hacer el login segun los datos recogidos en el formulario y lo guarda en el objeto tipo Usuario
     * @param value Valores recogidos por el formulario de acceso
     * @returns
     */
    signIn(value) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            this.userSvc.getUserByEmail(value.email).subscribe((resultado) => {
                this.usuarioLogado = resultado;
            });
            return this.angularFireAuth.signInWithEmailAndPassword(value.email, value.password);
        });
    }
    /**
     * Método de deslogado del usuario con retorno al Home
     * @returns
     */
    signOut() {
        return new Promise((resolve, reject) => {
            this.angularFireAuth.signOut();
            this.userEmail = null;
            this.router.navigateByUrl('home');
        });
    }
    /**
     * Obtiene los detalles de usuario a traves del Auth de Firebase
     * @returns
     */
    userDetails() {
        return this.angularFireAuth.user;
    }
    /**
     * Método que activa o desactiva rutas en funcion de si existe usuarios logado o no, para personalizar el menú
     * Si no estas logado, redireccionaria al Login
     * @param next Ruta a la que se intenta acceder
     * @param state Estado del enrutador
     * @returns
     */
    canActivate(next, state) {
        return this.angularFireAuth.authState.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.take)(1), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((user) => !!user), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.tap)((loggedIn) => {
            if (!loggedIn) {
                this.router.navigate(['/login']);
            }
        }));
    }
    /**
     * Comprueba si hay un usuario logado o no
     * @returns
     */
    checkLogin() {
        return !!this.angularFireAuth.currentUser;
    }
    /**
     * Obtiene el mail del usuario en caso de estar logado
     * @returns
     */
    getUserEmail() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            if (this.checkLogin()) {
                const user = yield this.angularFireAuth.currentUser;
                if (user) {
                    this.userEmail = user.email;
                    return this.userEmail || '';
                }
            }
            return '';
        });
    }
    initAuthStateListener() {
        this.angularFireAuth.onAuthStateChanged((user) => {
            if (user) {
                this.userEmail = user.email;
            }
            else {
                this.userEmail = '';
            }
        });
    }
    /**
     * Método para cambiar la contraseña del usuario
     * @param newPass String recibido a través del formulario con la nueva contraseña
     */
    passChange(newPass) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const user = yield this.angularFireAuth.currentUser;
            yield user.updatePassword(newPass);
        });
    }
    /**
     * Metodo para eliminar un usuario concreto
     * @param userId id del usuario a eliminar pasado por parámetro
     */
    deleteUser(userId) {
        this.firestore
            .collection('usuarios')
            .doc(userId)
            .delete()
            .then(() => {
            this.avisosSvc.presentToast('Usuario eliminado correctamente', 'success');
        })
            .catch((error) => {
            this.avisosSvc.presentToast('Error eliminando el Usuario', 'danger');
        });
    }
    deleteAccount() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            try {
                const user = this.angularFireAuth.currentUser;
                yield (yield user).delete();
                this.deleteUser(this.usuarioLogado.ID);
                this.router.navigateByUrl('home');
            }
            catch (error) {
                console.log('Error deleting account', error);
            }
        });
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_6__.AngularFireAuth },
    { type: _user_service__WEBPACK_IMPORTED_MODULE_0__.UserService },
    { type: _angular_fire_compat_firestore___WEBPACK_IMPORTED_MODULE_7__.AngularFirestore },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _avisos_service__WEBPACK_IMPORTED_MODULE_1__.AvisosService }
];
AuthService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)({
        providedIn: 'root',
    })
], AuthService);



/***/ }),

/***/ 9595:
/*!********************************************!*\
  !*** ./src/app/services/avisos.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AvisosService": () => (/* binding */ AvisosService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 3819);



let AvisosService = class AvisosService {
    /**
     * Constructor de clase
     * @param toastCtrl controlador del componente Toast que muestra los avisos
     */
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    /**
     * Muestra el toast con el mensaje y color pasado por parámetro
     * @param mensaje String que con el mensaje a mostrar
     * @param estado String con el resultado. Success para correcto y Danger para Error. Se puede indicar cualquier otro color de Ionic
     */
    presentToast(mensaje, estado) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: mensaje,
                color: estado,
                duration: 3000,
            });
            yield toast.present();
        });
    }
};
AvisosService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.ToastController }
];
AvisosService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], AvisosService);



/***/ }),

/***/ 3071:
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": () => (/* binding */ UserService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_fire_compat_firestore___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/compat/firestore/ */ 2393);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 6942);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 4505);






let UserService = class UserService {
    /**
     * Constructor de clase
     * @param firestore Servicio de Firestore
     * @param http Servicio de http usado para peticiones json a los datos del menu
     */
    constructor(firestore, http) {
        this.firestore = firestore;
        this.http = http;
        this.userEmail = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject('');
        /**
         * variable que almacena la URL del nuevo avatar para cambiarlo
         */
        this.newAvatar = '';
    }
    setUserEmail(email) {
        this.userEmail.next(email);
    }
    getUserEmail() {
        return this.userEmail.asObservable();
    }
    /**
     * Obtiene un observable con todos los usuarios
     * @returns
     */
    getUsers() {
        return this.firestore.collection('usuarios').valueChanges({ idField: 'id' });
    }
    /**
     * Obtiene un Observable del usuario a traves de su email
     * @param email Parametro pasado para buscar al usuario
     * @returns
     */
    getUserByEmail(email) {
        return this.firestore.collection('usuarios', ref => ref.where('EMAIL', '==', email).limit(1))
            .valueChanges({ idField: 'id' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(usuarios => usuarios[0]));
    }
    /**
     * Devuelve un observable con el usuario buscado por Id
     * @param userId Id del usuario a buscar
     * @returns
     */
    getUserById(userId) {
        return this.firestore.collection('usuarios', ref => ref.where('ID', '==', userId).limit(1))
            .valueChanges({ idField: 'id' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(usuarios => usuarios[0]));
    }
    /**
     * Metodo que actualia el total de puntos del usuario
     * @param email Email del usuario a actualizar
     * @param puntos puntos totales del usuario
     */
    updateUserPuntos(email, puntos) {
        this.getUserByEmail(email).subscribe(user => {
            return this.firestore.collection('usuarios').doc(user.ID).update({
                PUNTOS: puntos
            });
        });
    }
    /**
     * Metodo para cambiar de rol al usuario: Retador <=> Administrador
     * @param userId Id del usuario al que cambiar el Rol
     * @returns
     */
    updateUserRol(userId) {
        this.getUserById(userId).subscribe(usuario => {
            this.usuario = usuario;
        });
        if (this.usuario.ROL == 'retador') {
            this.newRol = 'admin';
            console.log('cambio a', this.newRol);
        }
        else {
            this.newRol = 'retador';
            console.log('cambio a', this.newRol);
        }
        return this.firestore.collection('usuarios').doc(userId).update({
            ROL: this.newRol
        });
    }
    /**
     * Metodo para cambiar el Avatar de un usuario
     * @param userId Id del usuario al que cambiar el Avatar
     * @param avatar Imagen nueva del avatar (string saneado)
     * @returns
     */
    updateUserAvatar(userId, avatar) {
        this.getUserById(userId).subscribe(usuario => {
            this.usuario = usuario;
            this.newAvatar = this.usuario.AVATAR;
            this.newAvatar = avatar;
        });
        return this.firestore.collection('usuarios').doc(userId).update({
            AVATAR: avatar
        });
    }
    /**
     * Obtiene el total de puntos de un usuario concreto
     * @param user Usuario del que obtener los puntos
     * @returns
     */
    getTotalPuntosByUser(user) {
        return this.firestore.collection('retosconseguidos', ref => ref.where('USER', '==', user))
            .valueChanges({ idField: 'ID' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(retos => retos.reduce((total, reto) => total + reto.PUNTOS, 0)));
    }
    /**
     * Obtiene una lista ordenada de los usuarios con más puntos. El de mayor puntuacion, arriba
     * @returns
     */
    getUsuariosPorPuntos() {
        return this.firestore.collection('usuarios', ref => ref.orderBy('PUNTOS', 'desc'))
            .valueChanges({ idField: 'ID' });
    }
    /**
     * Obtiene del JSON de roles las opciones adecuadas dependiendo del rol del usuario logado
     * @param roles roles del usuario logado
     * @returns
     */
    getMenuOpts(roles) {
        return this.http.get('/assets/data/menu.json')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((menuOpts) => {
            return menuOpts.filter((menuOpt) => {
                return roles.includes(menuOpt.rol);
            });
        }));
    }
};
UserService.ctorParameters = () => [
    { type: _angular_fire_compat_firestore___WEBPACK_IMPORTED_MODULE_2__.AngularFirestore },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient }
];
UserService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], UserService);



/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
/**
 * This file can be replaced during build by using the `fileReplacements` array.
 *`ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
 * The list of file replacements can be found in `angular.json`.
 */
const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyDxKCI4NsPwemIMnkkgGx1kgrmZFD23GXk',
        authDomain: 'ionicfirebase-11be8.firebaseapp.com',
        databaseURL: 'https://ionicfirebase-11be8-default-rtdb.europe-west1.firebasedatabase.app',
        projectId: 'ionicfirebase-11be8',
        storageBucket: 'ionicfirebase-11be8.appspot.com',
        messagingSenderId: '1075018770032',
        appId: '1:1075018770032:web:0a384eadfcff08237bf6b5',
        measurementId: 'G-X9JBW696P1',
    },
    news: {
        apiKey: '392c4ef2447040d1aab42bffac29c335',
        apiUrl: 'https://newsapi.org/v2',
    },
};
/**
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 8150);
/* harmony import */ var _ionic_pwa_elements_loader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/pwa-elements/loader */ 8763);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app/app.module */ 6747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environments/environment */ 2340);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_4__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_1__.AppModule)
    .catch(err => console.log(err));
(0,_ionic_pwa_elements_loader__WEBPACK_IMPORTED_MODULE_0__.defineCustomElements)(window);


/***/ }),

/***/ 863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-accordion_2.entry.js": [
		79,
		"common",
		"node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js"
	],
	"./ion-action-sheet.entry.js": [
		5593,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		3225,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		4812,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		6655,
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		4856,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		3059,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-breadcrumb_2.entry.js": [
		8648,
		"common",
		"node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js"
	],
	"./ion-button_2.entry.js": [
		8308,
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		4690,
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		4090,
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		6214,
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		9447,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime-button.entry.js": [
		7950,
		"default-node_modules_ionic_core_dist_esm_parse-71f28cd7_js-node_modules_ionic_core_dist_esm_t-0c999b",
		"node_modules_ionic_core_dist_esm_ion-datetime-button_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		9689,
		"default-node_modules_ionic_core_dist_esm_parse-71f28cd7_js-node_modules_ionic_core_dist_esm_t-0c999b",
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		8840,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		749,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		9667,
		"common",
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		3288,
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		5473,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		3634,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		2855,
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		495,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		8737,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		9632,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-picker-column-internal.entry.js": [
		4446,
		"common",
		"node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js"
	],
	"./ion-picker-internal.entry.js": [
		2275,
		"node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js"
	],
	"./ion-popover.entry.js": [
		8050,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		8994,
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		3592,
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		5454,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		290,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		2666,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		4816,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		5534,
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		4902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		1938,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		8179,
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		668,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		1624,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		9989,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		8902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		199,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		8395,
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		6357,
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		8268,
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		5269,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		2875,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 5899:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/pwa-elements/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \**************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./pwa-action-sheet.entry.js": [
		5464,
		"node_modules_ionic_pwa-elements_dist_esm_pwa-action-sheet_entry_js"
	],
	"./pwa-camera-modal-instance.entry.js": [
		8724,
		"node_modules_ionic_pwa-elements_dist_esm_pwa-camera-modal-instance_entry_js"
	],
	"./pwa-camera-modal.entry.js": [
		8145,
		"node_modules_ionic_pwa-elements_dist_esm_pwa-camera-modal_entry_js"
	],
	"./pwa-camera.entry.js": [
		527,
		"node_modules_ionic_pwa-elements_dist_esm_pwa-camera_entry_js"
	],
	"./pwa-toast.entry.js": [
		4389,
		"node_modules_ionic_pwa-elements_dist_esm_pwa-toast_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 5899;
module.exports = webpackAsyncContext;

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map