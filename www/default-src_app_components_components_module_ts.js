"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_components_components_module_ts"],{

/***/ 8950:
/*!**************************************************************************!*\
  !*** ./src/app/components/fab-login/fab-login.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = "ion-fab-button {\n  --border-radius: 15px;\n  --box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, .3), 0px 1px 3px 1px rgba(0, 0, 0, .15);\n  --color: black;\n}\n\nion-modal {\n  --height: 50%;\n  --border-radius: 16px;\n  --box-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);\n}\n\nion-modal::part(backdrop) {\n  background: rgb(209, 213, 219);\n  opacity: 1;\n}\n\nion-modal ion-toolbar {\n  --color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZhYi1sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHFCQUFBO0VBQ0EsbUZBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EscUJBQUE7RUFDQSxnRkFBQTtBQUNGOztBQUVBO0VBQ0UsOEJBQUE7RUFDQSxVQUFBO0FBQ0Y7O0FBRUE7RUFFRSxjQUFBO0FBQUYiLCJmaWxlIjoiZmFiLWxvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWZhYi1idXR0b24ge1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAtLWJveC1zaGFkb3c6IDBweCAxcHggMnB4IDBweCByZ2JhKDAsIDAsIDAsIC4zKSwgMHB4IDFweCAzcHggMXB4IHJnYmEoMCwgMCwgMCwgLjE1KTtcclxuICAtLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuaW9uLW1vZGFsIHtcclxuICAtLWhlaWdodDogNTAlO1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMTZweDtcclxuICAtLWJveC1zaGFkb3c6IDAgMTBweCAxNXB4IC0zcHggcmdiKDAgMCAwIC8gMC4xKSwgMCA0cHggNnB4IC00cHggcmdiKDAgMCAwIC8gMC4xKTtcclxufVxyXG5cclxuaW9uLW1vZGFsOjpwYXJ0KGJhY2tkcm9wKSB7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgyMDksIDIxMywgMjE5KTtcclxuICBvcGFjaXR5OiAxO1xyXG59XHJcblxyXG5pb24tbW9kYWwgaW9uLXRvb2xiYXIge1xyXG4gIFxyXG4gIC0tY29sb3I6IHdoaXRlO1xyXG59Il19 */";

/***/ }),

/***/ 5413:
/*!********************************************************************!*\
  !*** ./src/app/components/header/header.component.scss?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 6866:
/*!**************************************************************************!*\
  !*** ./src/app/components/info-reto/info-reto.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = ".titulo {\n  width: 100%;\n  position: absolute;\n  color: rgb(252, 163, 17);\n  background-color: black;\n  padding: 5px 5px 40px 5px;\n}\n\n.titulo h1 {\n  position: absolute;\n  top: 28px;\n  left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZm8tcmV0by5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsdUJBQUE7RUFDQSx5QkFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtBQUNGIiwiZmlsZSI6ImluZm8tcmV0by5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50aXR1bG8ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBjb2xvcjogcmdiKDI1MiwgMTYzLCAxNyk7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgcGFkZGluZzogNXB4IDVweCA0MHB4IDVweDtcclxufVxyXG5cclxuLnRpdHVsbyBoMSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMjhweDtcclxuICBsZWZ0OiA1cHg7XHJcbn1cclxuIl19 */";

/***/ }),

/***/ 9436:
/*!******************************************************************!*\
  !*** ./src/app/components/login/login.component.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "@charset \"UTF-8\";\n/* Añade este estilo en tu archivo CSS global o en la sección de estilos de tu componente */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUFoQiwyRkFBQSIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEHDsWFkZSBlc3RlIGVzdGlsbyBlbiB0dSBhcmNoaXZvIENTUyBnbG9iYWwgbyBlbiBsYSBzZWNjacOzbiBkZSBlc3RpbG9zIGRlIHR1IGNvbXBvbmVudGUgKi9cclxuLmN1c3RvbS1jb250ZW50IHtcclxufVxyXG4iXX0= */";

/***/ }),

/***/ 1346:
/*!****************************************************************!*\
  !*** ./src/app/components/menu/menu.component.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "ion-fab-button {\n  --border-radius: 15px;\n  --box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.3),\n    0px 1px 3px 1px rgba(0, 0, 0, 0.15);\n  --color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1lbnUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtFQUNBO3VDQUFBO0VBRUEsY0FBQTtBQUNGIiwiZmlsZSI6Im1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tZmFiLWJ1dHRvbiB7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIC0tYm94LXNoYWRvdzogMHB4IDFweCAycHggMHB4IHJnYmEoMCwgMCwgMCwgMC4zKSxcclxuICAgIDBweCAxcHggM3B4IDFweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xyXG4gIC0tY29sb3I6IGJsYWNrO1xyXG59XHJcbiJdfQ== */";

/***/ }),

/***/ 4758:
/*!************************************************************************!*\
  !*** ./src/app/components/new-reto/new-reto.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJuZXctcmV0by5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 6341:
/*!**********************************************************************!*\
  !*** ./src/app/components/noticia/noticia.component.scss?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub3RpY2lhLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 7622:
/*!************************************************************************!*\
  !*** ./src/app/components/noticias/noticias.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub3RpY2lhcy5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 1483:
/*!************************************************************************!*\
  !*** ./src/app/components/registro/registro.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWdpc3Ryby5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 7266:
/*!****************************************************************!*\
  !*** ./src/app/components/reto/reto.component.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXRvLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 6053:
/*!******************************************************************!*\
  !*** ./src/app/components/retos/retos.component.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXRvcy5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 9288:
/*!**************************************************************************!*\
  !*** ./src/app/components/fab-login/fab-login.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = "<ion-fab slot=\"fixed\" horizontal=\"end\" vertical=\"bottom\" class=\"ion-margin\" class=\"login-fixed\">\r\n  <ion-fab-button id=\"open-modal\" color=\"primary\" (click)=\"loginModal()\">\r\n    <ion-icon name=\"log-in-outline\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>\r\n";

/***/ }),

/***/ 2011:
/*!********************************************************************!*\
  !*** ./src/app/components/header/header.component.html?ngResource ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = "<ion-header class=\"ion-no-border\" translucent>\r\n  <ion-toolbar>\r\n\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\" text=\"Regresar\" color=\"primary\"></ion-back-button>\r\n    </ion-buttons>\r\n\r\n    <ion-title class=\"ion-text-capitalize\" style=\"font-family: 'Nasa'\">{{ titulo }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>";

/***/ }),

/***/ 8201:
/*!**************************************************************************!*\
  !*** ./src/app/components/info-reto/info-reto.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"secondary\">\r\n    <ion-title color=\"primary\">{{ reto?.TITULO }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <ion-img [src]=\"reto?.IMAGEN\"></ion-img>\r\n\r\n    <ion-card-content>\r\n      <ion-item>\r\n        <ion-icon slot=\"start\" [name]=\"getIconTipo(reto?.TIPO)\"></ion-icon>\r\n        <ion-label>Tipo: {{ reto?.TIPO }}</ion-label>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-icon\r\n          name=\"ellipse-sharp\"\r\n          [color]=\"getColorNivel(reto?.NIVEL)\"\r\n          slot=\"start\"\r\n        ></ion-icon>\r\n        <ion-label [color]=\"getColorNivel(reto?.NIVEL)\">\r\n          Nivel: {{ reto?.NIVEL }}\r\n        </ion-label>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-icon\r\n          name=\"star-sharp\"\r\n          slot=\"start\"\r\n          [color]=\"esFavorito ? 'primary' : 'medium'\"\r\n          (click)=\"toggleFavorito()\"\r\n        ></ion-icon>\r\n        <ion-icon\r\n          name=\"share-social-outline\"\r\n          slot=\"end\"\r\n          color=\"primary\"\r\n          (click)=\"compartirReto()\"\r\n        ></ion-icon>\r\n        <ion-icon\r\n          name=\"checkmark-circle-sharp\"\r\n          slot=\"start\"\r\n          [color]=\"esRetoConseguido ? 'success' : 'medium'\"\r\n          (click)=\"toggleRetoConseguido()\"\r\n        ></ion-icon>\r\n      </ion-item>\r\n\r\n      <ion-label>\r\n        {{ reto?.DESCRIPCION | slice : 0 : oculto }}...\r\n        <ion-button (click)=\"mostrarDescripcion()\" fill=\"clear\" size=\"small\">\r\n          Leer más\r\n        </ion-button>\r\n      </ion-label>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-button\r\n    (click)=\"cierreModal()\"\r\n    expand=\"block\"\r\n    fill=\"outline\"\r\n    shape=\"none\"\r\n  >\r\n    Regresar\r\n  </ion-button>\r\n</ion-footer>\r\n";

/***/ }),

/***/ 4437:
/*!******************************************************************!*\
  !*** ./src/app/components/login/login.component.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-content class=\"ion-padding custom-content\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Login</ion-title>\r\n  </ion-toolbar>\r\n\r\n  <form\r\n    class=\"form\"\r\n    [formGroup]=\"loginForm\"\r\n    (ngSubmit)=\"logIn(loginForm.value); closeModal()\"\r\n  >\r\n    <ion-item>\r\n      <ion-label position=\"floating\">Email</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"email\"></ion-input>\r\n    </ion-item>\r\n\r\n    <!-- <ng-container *ngFor=\"let error of errorMsg\">\r\n      <div\r\n        *ngIf=\"loginForm.get('email').hasError( error.email[0].type) && (loginForm.get('email').dirty || loginForm.get('email').touched)\">\r\n        {{ error.email[0].message }}\r\n      </div>\r\n    </ng-container> -->\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Password</ion-label>\r\n      <ion-input\r\n        type=\"password\"\r\n        formControlName=\"password\"\r\n        class=\"form-control\"\r\n        required\r\n      ></ion-input>\r\n    </ion-item>\r\n\r\n    <!--   <ng-container *ngFor=\"let error of errorMsg\">\r\n      <div\r\n        *ngIf=\"loginForm.get('password').hasError( error.password[1].type) && (loginForm.get('password').dirty || loginForm.get('password').touched)\">\r\n        {{ error.password[1].message }}\r\n      </div>\r\n    </ng-container> -->\r\n\r\n    <ion-button type=\"submit\" expand=\"block\" [disabled]=\"!loginForm.valid\"\r\n      >Log in</ion-button\r\n    >\r\n\r\n    <!--  <ion-button (click)=\"gAuth()\" expand=\"block\" fill=\"clear\" shape=\"round\" disabled=\"true\">\r\n      Login con Google\r\n    </ion-button> -->\r\n\r\n    <label class=\"ion-text-center\" color=\"danger\">{{ errorMsg }}</label>\r\n  </form>\r\n  <p class=\"ion-text-center\">\r\n    Aun no registrado?<a (click)=\"registroModal(); closeModal()\">Registrar</a>\r\n  </p>\r\n</ion-content>\r\n";

/***/ }),

/***/ 2574:
/*!****************************************************************!*\
  !*** ./src/app/components/menu/menu.component.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<ion-fab\r\n  vertical=\"bottom\"\r\n  horizontal=\"start\"\r\n  slot=\"fixed\"\r\n  class=\"ion-margin\"\r\n  class=\"fab-fixed\"\r\n>\r\n  <ion-fab-button>\r\n    <ion-icon name=\"add\"></ion-icon>\r\n  </ion-fab-button>\r\n  <ion-fab-list side=\"top\">\r\n    <ion-fab-button *ngFor=\"let opt of menuOpts; let i = index\" color=\"primary\">\r\n      <ion-icon\r\n        [name]=\"opt.icon\"\r\n        (click)=\"navigateToPage(opt.redirectTo)\"\r\n        color=\"secondary\"\r\n      ></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab-list>\r\n</ion-fab>\r\n";

/***/ }),

/***/ 1540:
/*!************************************************************************!*\
  !*** ./src/app/components/new-reto/new-reto.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title color=\"primary\">Nuevo Reto</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <form [formGroup]=\"newRetoForm\" (ngSubmit)=\"crearReto(newRetoForm.value)\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">Título <ion-text color=\"danger\">*</ion-text></ion-label>\r\n      <ion-input formControlName=\"TITULO\" type=\"text\" required></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">Descripción <ion-text color=\"danger\">*</ion-text></ion-label>\r\n      <ion-textarea formControlName=\"DESCRIPCION\" type=\"text\" autocapitalize=\"sentences\" [autoGrow]=\"true\"\r\n        required></ion-textarea>\r\n    </ion-item>\r\n    <ion-grid fixed>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          <ion-item>\r\n            <ion-label>Tipo</ion-label>\r\n            <ion-select formControlName=\"TIPO\" value=\"ocular\" multiple=\"false\" placeholder=\"Tipo de Reto\" required>\r\n              <ion-select-option value=\"ocular\">Ocular</ion-select-option>\r\n              <ion-select-option value=\"prismaticos\">Prismaticos</ion-select-option>\r\n              <ion-select-option value=\"telescopio\">Telescopio</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n        </ion-col>\r\n        <ion-col size=\"6\">\r\n          <ion-item>\r\n            <ion-label>Nivel</ion-label>\r\n            <ion-select formControlName=\"NIVEL\" value=\"facil\" multiple=\"false\" placeholder=\"Nivel de dificultad\"\r\n              required>\r\n              <ion-select-option value=\"facil\">Facil</ion-select-option>\r\n              <ion-select-option value=\"intermedio\">Intermedio</ion-select-option>\r\n              <ion-select-option value=\"dificil\">Dificil</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <ion-button (click)=\"selectImage()\" expand=\"block\" fill=\"clear\" shape=\"round\" color=\"primary\">\r\n      Seleccionar imagen\r\n    </ion-button>\r\n    <ion-item *ngIf=\"image\">\r\n      <ion-img [src]=\"image\"></ion-img>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-checkbox formControlName=\"ACTIVO\" slot=\"start\" checked=\"true\"></ion-checkbox>\r\n      <ion-label>Marcar Reto como Activo</ion-label>\r\n    </ion-item>\r\n    <ion-button type=\"submit\" expand=\"block\" [disabled]=\"!newRetoForm.valid\" color=\"primary\">Crear Reto</ion-button>\r\n  </form>\r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n\r\n\r\n  <ion-button (click)=\"closeModal()\" expand=\"block\" fill=\"clear\" shape=\"round\">Cancelar</ion-button>\r\n</ion-footer>";

/***/ }),

/***/ 4248:
/*!**********************************************************************!*\
  !*** ./src/app/components/noticia/noticia.component.html?ngResource ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = "<ion-card>\r\n  <ion-card-subtitle class=\"ion-margin\">\r\n\r\n    <ion-row class=\"ion-align-items-center\">\r\n\r\n      <ion-col class=\"ion-margin\" (click)=\"openArticle()\">\r\n        <span class=\"article-source-name\">{{ article.source.name }}</span>\r\n      </ion-col>\r\n      <ion-col align=\"right\">\r\n        <ion-button (click)=\"onOpenMenu()\" fill=\"clear\">\r\n          <ion-icon slot=\"icon-only\" name=\"ellipsis-vertical-outline\"></ion-icon>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </ion-card-subtitle>\r\n\r\n  <ion-card-title class=\"ion-padding-horizontal ion-margin-bottom\" (click)=\"openArticle()\">\r\n\r\n    {{ article.title }}\r\n  </ion-card-title>\r\n\r\n  <ion-img *ngIf=\"article.urlToImage\" [src]=\"article.urlToImage\" (click)=\"openArticle()\"></ion-img>\r\n\r\n  <ion-card-content *ngIf=\" article.description \">\r\n    <p> {{ article.description }}</p>\r\n  </ion-card-content>\r\n\r\n</ion-card>";

/***/ }),

/***/ 1412:
/*!************************************************************************!*\
  !*** ./src/app/components/noticias/noticias.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<ion-grid fixed>\r\n\r\n  <ion-row>\r\n    <ion-col *ngFor=\"let article of articles; let i = index\"\r\n    size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\r\n     <app-noticia [article]=\"article\" [index]=\"i\">\r\n     </app-noticia>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-grid>";

/***/ }),

/***/ 1342:
/*!************************************************************************!*\
  !*** ./src/app/components/registro/registro.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>Registro</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form class=\"form\" [formGroup]=\"registroForm\" (ngSubmit)=\"registro(registroForm.value)\">\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">Nombre</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"NOMBRE\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">Email</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"EMAIL\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ng-container *ngFor=\"let error of errorMsg\">\r\n      <div *ngIf=\"registroForm.get('EMAIL').hasError( error.email[0].type) && (registroForm.get('EMAIL').dirty || registroForm.get('EMAIL').touched)\">\r\n        {{  error.email[0].message }}\r\n      </div>\r\n    </ng-container>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Password</ion-label>\r\n      <ion-input type=\"password\" formControlName=\"PASSWORD\" class=\"form-controll\" required></ion-input>\r\n    </ion-item>\r\n    \r\n    <ng-container *ngFor=\"let error of errorMsg\">\r\n        <div *ngIf=\"registroForm.get('PASSWORD').hasError(error.password[1].type) && (registroForm.get('PASSWORD').dirty || registroForm.get('PASSWORD').touched)\">\r\n          {{ error.password[1].message }}\r\n        </div>\r\n    </ng-container>\r\n\r\n    <ion-button type=\"submit\" expand=\"block\" [disabled]=\"!registroForm.valid\">Registrar</ion-button>\r\n    \r\n    <label class=\"ion-text-center\" color=\"danger\">{{errorMsg}}</label>\r\n    <label class=\"ion-text-center\" color=\"success\">{{successMsg}}</label>\r\n  </form>\r\n  \r\n</ion-content>";

/***/ }),

/***/ 6585:
/*!****************************************************************!*\
  !*** ./src/app/components/reto/reto.component.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = "<ion-card>\r\n  <ion-card-header>\r\n    <ion-item>\r\n      <ion-card-subtitle color=\"primary\"> {{ reto.TITULO }} </ion-card-subtitle>\r\n      <ion-icon\r\n        name=\"information-circle-outline\"\r\n        color=\"primary\"\r\n        slot=\"end\"\r\n        (click)=\"verDetalle(reto.ID)\"\r\n      ></ion-icon>\r\n    </ion-item>\r\n  </ion-card-header>\r\n\r\n  <ion-card-content>\r\n    <ion-item>\r\n      <ion-img [src]=\"reto.IMAGEN\"></ion-img>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-icon\r\n        [name]=\"getIconTipo(reto.TIPO)\"\r\n        slot=\"start\"\r\n        color=\"primary\"\r\n      ></ion-icon>\r\n      <ion-icon\r\n        name=\"ellipse-sharp\"\r\n        [color]=\"getColorNivel(reto.NIVEL)\"\r\n        slot=\"start\"\r\n      ></ion-icon>\r\n\r\n      <ion-icon\r\n        *ngIf=\"esRetoConseguido\"\r\n        name=\"checkmark-circle-sharp\"\r\n        slot=\"start\"\r\n        color=\"success\"\r\n        (click)=\"removeRetoConseguido(reto.ID, this.userEmail)\"\r\n      ></ion-icon>\r\n      <ion-icon\r\n        *ngIf=\"!esRetoConseguido\"\r\n        name=\"checkmark-circle-outline\"\r\n        slot=\"start\"\r\n        color=\"success\"\r\n        (click)=\"setRetoConseguido(reto.ID, this.userEmail)\"\r\n      ></ion-icon>\r\n\r\n      <ion-icon\r\n        *ngIf=\"esFavorito\"\r\n        name=\"star-sharp\"\r\n        slot=\"end\"\r\n        color=\"primary\"\r\n        (click)=\"quitarFavorito(reto.ID, this.userEmail)\"\r\n      ></ion-icon>\r\n      <ion-icon\r\n        *ngIf=\"!esFavorito\"\r\n        name=\"star-outline\"\r\n        slot=\"end\"\r\n        color=\"primary\"\r\n        (click)=\"setFavorito(reto.ID, this.userEmail)\"\r\n      ></ion-icon>\r\n\r\n      <ion-buttons slot=\"end\">\r\n        <ion-button slot=\"icon-only\" (click)=\"compartirReto()\">\r\n          <ion-icon\r\n            name=\"share-social-outline\"\r\n            color=\"primary\"\r\n            slot=\"end\"\r\n          ></ion-icon>\r\n        </ion-button>\r\n      </ion-buttons>\r\n    </ion-item>\r\n  </ion-card-content>\r\n</ion-card>\r\n";

/***/ }),

/***/ 6894:
/*!******************************************************************!*\
  !*** ./src/app/components/retos/retos.component.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-grid fixed>\r\n  <ion-row>\r\n    <ion-col *ngFor=\"let reto of retos; let i = index\"\r\n    size=\"12\" size-lg=\"3\" size-md=\"4\" size-sm=\"6\" size-xs=\"12\">\r\n     <app-reto [reto]=\"reto\" [index]=\"i\">\r\n     </app-reto>\r\n    </ion-col>\r\n\r\n  </ion-row>\r\n</ion-grid>";

/***/ }),

/***/ 5642:
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ComponentsModule": () => (/* binding */ ComponentsModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _fab_login_fab_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fab-login/fab-login.component */ 520);
/* harmony import */ var _registro_registro_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registro/registro.component */ 43);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ 7143);
/* harmony import */ var _noticia_noticia_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./noticia/noticia.component */ 4217);
/* harmony import */ var _noticias_noticias_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./noticias/noticias.component */ 4101);
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu/menu.component */ 5819);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./header/header.component */ 3646);
/* harmony import */ var _reto_reto_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reto/reto.component */ 9860);
/* harmony import */ var _info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./info-reto/info-reto.component */ 8083);
/* harmony import */ var _new_reto_new_reto_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./new-reto/new-reto.component */ 6356);
/* harmony import */ var _retos_retos_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./retos/retos.component */ 1255);





//imports de Componentes personalizados











let ComponentsModule = class ComponentsModule {
};
ComponentsModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [_fab_login_fab_login_component__WEBPACK_IMPORTED_MODULE_0__.FabLoginComponent,
            _registro_registro_component__WEBPACK_IMPORTED_MODULE_1__.RegistroComponent,
            _login_login_component__WEBPACK_IMPORTED_MODULE_2__.LoginComponent,
            _noticia_noticia_component__WEBPACK_IMPORTED_MODULE_3__.NoticiaComponent,
            _noticias_noticias_component__WEBPACK_IMPORTED_MODULE_4__.NoticiasComponent,
            _menu_menu_component__WEBPACK_IMPORTED_MODULE_5__.MenuComponent,
            _header_header_component__WEBPACK_IMPORTED_MODULE_6__.HeaderComponent,
            _reto_reto_component__WEBPACK_IMPORTED_MODULE_7__.RetoComponent,
            _retos_retos_component__WEBPACK_IMPORTED_MODULE_10__.RetosComponent,
            _info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_8__.InfoRetoComponent,
            _new_reto_new_reto_component__WEBPACK_IMPORTED_MODULE_9__.NewRetoComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_13__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_15__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_15__.ReactiveFormsModule,
        ], exports: [
            _fab_login_fab_login_component__WEBPACK_IMPORTED_MODULE_0__.FabLoginComponent,
            _registro_registro_component__WEBPACK_IMPORTED_MODULE_1__.RegistroComponent,
            _login_login_component__WEBPACK_IMPORTED_MODULE_2__.LoginComponent,
            _noticias_noticias_component__WEBPACK_IMPORTED_MODULE_4__.NoticiasComponent,
            _menu_menu_component__WEBPACK_IMPORTED_MODULE_5__.MenuComponent,
            _header_header_component__WEBPACK_IMPORTED_MODULE_6__.HeaderComponent,
            _reto_reto_component__WEBPACK_IMPORTED_MODULE_7__.RetoComponent,
            _retos_retos_component__WEBPACK_IMPORTED_MODULE_10__.RetosComponent,
            _info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_8__.InfoRetoComponent,
            _new_reto_new_reto_component__WEBPACK_IMPORTED_MODULE_9__.NewRetoComponent
        ]
    })
], ComponentsModule);



/***/ }),

/***/ 520:
/*!*************************************************************!*\
  !*** ./src/app/components/fab-login/fab-login.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FabLoginComponent": () => (/* binding */ FabLoginComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _fab_login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fab-login.component.html?ngResource */ 9288);
/* harmony import */ var _fab_login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fab-login.component.scss?ngResource */ 8950);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login/login.component */ 7143);



// Importar los módulos necesarios



let FabLoginComponent = class FabLoginComponent {
    /**
     * Constructor de la clase
     * @param modalCtrl modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
     *
     */
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
    }
    /**
     * Método de inicio que se ejecuta cuando el componente se inicializa
     * No hace nada en este caso
     */
    ngOnInit() { }
    /**
     * Método que abre la ventana de Login como un Modal
     * @returns {Promise<void>} Una promesa que se resuelve cuando el modal se presenta
     *
     */
    loginModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            // Crear el modal con el componente LoginComponent y una clase CSS personalizada
            const modal = yield this.modalCtrl.create({
                component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__.LoginComponent,
                cssClass: 'loginRegisterModal',
            });
            // Presentar el modal y devolver la promesa
            return yield modal.present();
        });
    }
};
FabLoginComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ModalController }
];
FabLoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-fab-login',
        template: _fab_login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_fab_login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FabLoginComponent);



/***/ }),

/***/ 3646:
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _header_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.component.html?ngResource */ 2011);
/* harmony import */ var _header_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.component.scss?ngResource */ 5413);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);



// Importar los módulos necesarios

let HeaderComponent = class HeaderComponent {
    /**
     * Constructor de la clase
     * No hace nada en este caso
     */
    constructor() {
        this.titulo = '';
    }
    /**
     * Método de inicio que se ejecuta cuando el componente se inicializa
     * No hace nada en este caso
     */
    ngOnInit() { }
};
HeaderComponent.ctorParameters = () => [];
HeaderComponent.propDecorators = {
    titulo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
HeaderComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-header',
        template: _header_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_header_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], HeaderComponent);



/***/ }),

/***/ 8083:
/*!*************************************************************!*\
  !*** ./src/app/components/info-reto/info-reto.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InfoRetoComponent": () => (/* binding */ InfoRetoComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _info_reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./info-reto.component.html?ngResource */ 8201);
/* harmony import */ var _info_reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./info-reto.component.scss?ngResource */ 6866);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 6436);
/* harmony import */ var src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/reto.service */ 1268);
/* harmony import */ var src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/avisos.service */ 9595);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user.service */ 3071);



// Importar los módulos necesarios







let InfoRetoComponent = class InfoRetoComponent {
    /**
     * Constructor de la clase
     * @param retoSvc Servicio que maneja la colección 'retos' de Firestore, que permite obtener, crear, modificar y eliminar retos
     * @param modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
     * @param socialSharing Plugin de Cordova que permite compartir contenido por diferentes redes sociales o aplicaciones
     * @param avisosSvc Servicio que muestra avisos de éxito o error al usuario mediante toast o alert
     * @param authSvc Servicio que controla el inicio de sesión y el registro de los usuarios mediante Firebase Authentication
     * @param userSvc Servicio que maneja la colección 'users' de Firestore, que permite obtener, crear, modificar y eliminar usuarios
     * @param platform Servicio de Ionic que permite acceder a la información de la plataforma donde se ejecuta la aplicación
     */
    constructor(retoSvc, modalCtrl, socialSharing, avisosSvc, authSvc, userSvc, platform) {
        this.retoSvc = retoSvc;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.avisosSvc = avisosSvc;
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.platform = platform;
        /**
         * Propiedad que almacena el número de caracteres a mostrar de la descripción del reto.
         * Por defecto es 150, pero se puede cambiar al pulsar el botón de mostrar más o mostrar menos.
         * @type {number} El número de caracteres
         */
        this.oculto = 150;
        /**
         * Propiedad que almacena el mensaje estándar para el botón de compartir en redes sociales.
         * @type {string} El mensaje
         */
        this.mensaje = 'Te reto!';
    }
    /**
     * Método de inicio que se ejecuta cuando el componente se inicializa.
     * Obtiene el email del usuario logado, el objeto reto con el id pasado por parámetro a través del input, y comprueba si el reto es favorito o conseguido por el usuario.
     */
    ngOnInit() {
        // Obtener el email del usuario logado
        this.authSvc.getUserEmail().then((email) => {
            this.email = email;
        });
        // Obtener el objeto reto con el id pasado por parámetro a través del input
        this.retoSvc.getRetosById(this.retoId).subscribe((retos) => {
            // Guardar el primer elemento del array de retos, que es el que coincide con el id
            this.reto = retos[0];
            // Comprobar si el reto es favorito o conseguido por el usuario
            this.checkRetoFavorito(this.reto.ID, this.email);
            this.checkRetoConseguido(this.reto.ID, this.email);
        });
    }
    /**
     * Método que asigna un icono dependiendo del tipo de reto pasado por parámetro.
     * @param tipo El tipo de reto
     * @returns {string} El nombre del icono de ionicons.io
     */
    getIconTipo(tipo) {
        // Usar un switch para asignar el icono según el tipo de reto
        switch (tipo) {
            case 'telescopio':
                return 'telescope-outline';
            case 'prismaticos':
                return 'recording-outline';
            case 'ocular':
                return 'eye-outline';
            default:
                return 'help';
        }
    }
    /**
     * Método que asigna un color al icono dependiendo del nivel de dificultad del reto
     * @param nivel Nivel de dificultad del Reto
     * @returns Retorna el color a poner en el icono
     */
    getColorNivel(nivel) {
        switch (nivel) {
            case 'facil':
                return 'success';
            case 'intermedio':
                return 'warning';
            case 'dificil':
                return 'danger';
            default:
                return 'help';
        }
    }
    /**
     * Método para compartir el reto en las aplicaciones disponibles en el terminal (Mail o RRSS).
     * Este método usa el plugin de SocialSharing, que permite compartir contenido por diferentes redes sociales o aplicaciones.
     * Para que funcione el plugin, se debe cumplir una de estas condiciones:
     * - La plataforma debe ser Android, ya que el plugin solo funciona en dispositivos Android.
     * - La plataforma debe soportar la API de navegador share, que permite compartir contenido desde el navegador web.
     */
    compartirReto() {
        // Comprobar si la plataforma es Android
        if (this.platform.is('android')) {
            // Usar el plugin de SocialSharing para compartir el mensaje
            this.socialSharing.share(this.mensaje);
        }
        else {
            // Comprobar si la plataforma soporta la API de navegador share
            if (navigator.share) {
                // Usar la API de navegador share para compartir el mensaje y la url
                navigator
                    .share({
                    text: this.mensaje,
                    url: '',
                })
                    // Mostrar un mensaje de éxito en la consola
                    .then(() => console.log('Compartido!'))
                    // Mostrar un mensaje de error en la consola
                    .catch((error) => console.log('Error compartiendo', error));
            }
        }
    }
    /**
     * Método que comprueba si el reto es favorito o no para el usuario logado.
     * Este método se suscribe al servicio `retoSvc`, que devuelve un valor booleano que indica si el reto existe o no en la colección `favoritos` de Firestore.
     * El valor se asigna a la propiedad `esFavorito`, que se usa para mostrar u ocultar el icono de favorito en el ion-card del reto.
     * @param retoId El id del reto
     * @param user El email del usuario
     */
    checkRetoFavorito(retoId, user) {
        // Suscribirse al servicio `retoSvc`, que devuelve un valor booleano que indica si el reto existe o no en la colección `favoritos` de Firestore
        this.retoSvc.checkFavorito(retoId, user).subscribe((existe) => {
            // Asignar el valor a la propiedad `esFavorito`
            this.esFavorito = existe;
            // Mostrar el valor en la consola
            console.log(this.esFavorito);
        });
    }
    /**
     * Método para marcar como Favorito un Reto concreto al usuario logado.
     * Este método llama al servicio `retoSvc`, que añade el reto a la colección `favoritos` de Firestore.
     * Al finalizar, muestra el toast con el mensaje de éxito o error, usando el servicio `avisosSvc`.
     * @param retoId El id del reto a marcar como Favorito
     * @param user El email del usuario que marca el Favorito
     */
    setFavorito(retoId, user) {
        // Usar un bloque try-catch para manejar posibles errores
        try {
            // Llamar al servicio `retoSvc`, que añade el reto a la colección `favoritos` de Firestore
            this.retoSvc.addFavorito(retoId, user);
            // Mostrar el toast con el mensaje de éxito, usando el servicio `avisosSvc`
            this.avisosSvc.presentToast('Favorito añadido', 'success');
        }
        catch (error) {
            // Mostrar el toast con el mensaje de error, usando el servicio `avisosSvc`
            this.avisosSvc.presentToast('Error al añadir Favorito', 'danger');
        }
    }
    /**
     * Método para quitar un Reto de la lista de Favoritos del usuario logado.
     * Este método usa el servicio de RetoService para obtener el id del favorito a partir del id del reto y el email del usuario, y luego eliminar el favorito de la colección de favoritos del usuario en Firestore.
     * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
     * @param {string} retoId El id del reto a quitar de Favoritos
     * @param {string} email El email del usuario que quita el Favorito
     * @returns {void}
     */
    quitarFavorito(retoId, email) {
        // Usar un bloque try-catch para manejar posibles errores
        try {
            // Obtener el id del favorito a partir del id del reto y el email del usuario usando el servicio de RetoService
            this.retoSvc.getFavorito(retoId, email).subscribe((fav) => {
                // Eliminar el favorito de la colección de favoritos del usuario usando el servicio de RetoService
                this.retoSvc.deleteFavorito(fav[0].ID_FAV);
            });
            // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
            this.avisosSvc.presentToast('Favorito eliminado correctamente', 'success');
        }
        catch (error) {
            // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
            this.avisosSvc.presentToast('Error al eliminar el Favorito', 'danger');
        }
    }
    /**
     * Método que comprueba si el reto es conseguido o no por el usuario logado.
     * Este método usa el servicio de RetoService para obtener un observable que indica si el reto existe o no en la colección de retos conseguidos del usuario.
     * El observable se suscribe y asigna el valor a la propiedad esRetoConseguido, que se usa para mostrar el icono de logro en la vista.
     * @param {string} retoId El id del reto a comprobar
     * @param {string} user El email del usuario logado
     * @returns {void}
     */
    checkRetoConseguido(retoId, user) {
        // Obtener el observable que indica si el reto es conseguido o no por el usuario logado usando el servicio de RetoService
        this.retoSvc.checkRetoConseguido(retoId, user).subscribe((existe) => {
            // Asignar el valor del observable a la propiedad esRetoConseguido
            this.esRetoConseguido = existe;
            // Mostrar el valor en la consola
            console.log(this.esRetoConseguido);
        });
    }
    /**
     * Método para marcar como Conseguido un Reto concreto por el usuario logado.
     * Este método usa el servicio de RetoService para añadir el reto a la colección de retos conseguidos del usuario en Firestore.
     * También usa el servicio de UserService para obtener el total de puntos del usuario y actualizarlo con los puntos del reto.
     * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
     * @param {string} retoId El id del reto a marcar como Conseguido
     * @param {string} user El email del usuario que marca el Conseguido
     * @returns {void}
     */
    setRetoConseguido(retoId, user) {
        // Usar un bloque try-catch para manejar posibles errores
        try {
            // Mostrar un mensaje en la consola indicando que se va a conseguir el reto
            console.log('vamos a conseguir el reto');
            // Añadir el reto a la colección de retos conseguidos del usuario usando el servicio de RetoService
            this.retoSvc.addRetoConseguido(retoId, user);
            // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
            this.avisosSvc.presentToast('Reto conseguido', 'success');
            // Obtener el total de puntos del usuario usando el servicio de UserService
            this.userSvc.getTotalPuntosByUser(this.email).subscribe((totalPuntos) => {
                // Actualizar el total de puntos del usuario con los puntos del reto usando el servicio de UserService
                this.userSvc.updateUserPuntos(this.email, totalPuntos);
            });
        }
        catch (error) {
            // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
            this.avisosSvc.presentToast('Error al conseguir Reto', 'danger');
        }
    }
    /**
     * Método para quitar un Reto de la lista de Retos Conseguidos del usuario logado.
     * Este método usa el servicio de RetoService para obtener el id del reto conseguido a partir del id del reto y el email del usuario, y luego eliminar el reto conseguido de la colección de retos conseguidos del usuario en Firestore.
     * Al finalizar, muestra el toast con el mensaje de éxito o error usando el servicio de AvisosService.
     * @param {string} retoId El id del reto a quitar de Retos Conseguidos
     * @param {string} email El email del usuario que quita el Conseguido
     * @returns {void}
     */
    removeRetoConseguido(retoId, email) {
        // Usar un bloque try-catch para manejar posibles errores
        try {
            // Obtener el id del reto conseguido a partir del id del reto y el email del usuario usando el servicio de RetoService
            this.retoSvc
                .getRetoConseguido(retoId, email)
                .subscribe((retoConseguido) => {
                // Eliminar el reto conseguido de la colección de retos conseguidos del usuario usando el servicio de RetoService
                this.retoSvc.deleteRetoConseguido(retoConseguido[0].ID_RETO_CONSEGUIDO);
                // Mostrar el toast con el mensaje de éxito usando el servicio de AvisosService
                this.avisosSvc.presentToast('Reto Conseguido eliminado', 'success');
            });
        }
        catch (error) {
            // Mostrar el toast con el mensaje de error usando el servicio de AvisosService
            this.avisosSvc.presentToast('Error al eliminar Reto', 'danger');
        }
    }
    /**
     * Método que cierra el modal para volver al listado de retos.
     * Este método usa el controlador de Modal de Ionic para cerrar el modal que muestra la información del reto.
     * @param {void}
     * @returns {void}
     */
    cierreModal() {
        // Cerrar el modal usando el controlador de Modal de Ionic
        this.modalCtrl.dismiss();
    }
};
InfoRetoComponent.ctorParameters = () => [
    { type: src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_3__.RetoService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_2__.SocialSharing },
    { type: src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_4__.AvisosService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__.AuthService },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__.UserService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.Platform }
];
InfoRetoComponent.propDecorators = {
    retoId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input }]
};
InfoRetoComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-info-reto',
        template: _info_reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_info_reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], InfoRetoComponent);



/***/ }),

/***/ 7143:
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginComponent": () => (/* binding */ LoginComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.component.html?ngResource */ 4437);
/* harmony import */ var _login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component.scss?ngResource */ 9436);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var _registro_registro_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../registro/registro.component */ 43);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user.service */ 3071);









let LoginComponent = class LoginComponent {
    /**
     * Constructor de la clase
     * @param authSvc Servicio para el manejo de la Autenticación con Firebase Auth, que permite iniciar sesión, registrarse, cerrar sesión y recuperar contraseña de los usuarios
     * @param userSvc Servicio que maneja la colección 'users' de Firestore, que permite obtener, crear, modificar y eliminar usuarios
     * @param fBuilder Servicio de Angular que permite crear formularios reactivos con validadores
     * @param modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
     * @param navCtrl Controlador de Navegación de Ionic, que permite navegar entre las páginas de la aplicación
     */
    constructor(authSvc, userSvc, fBuilder, modalCtrl, navCtrl) {
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.fBuilder = fBuilder;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        /**
         * Propiedad que almacena el texto a mostrar si el acceso es correcto.
         * @type {string} El texto de éxito
         */
        this.successMsg = '';
        /**
         * Propiedad que almacena el texto a mostrar si el acceso es erróneo.
         * @type {Error[]} El texto de error
         */
        this.errorMsg = [];
    }
    /**
     * Método de inicio que se ejecuta cuando el componente se inicializa.
     * Crea el formulario de login con los validadores necesarios para los campos de email y contraseña.
     */
    ngOnInit() {
        // Crear el formulario de login usando el servicio de FormBuilder
        this.loginForm = this.fBuilder.group({
            // Campo de email con validadores de requerido y formato de email
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
            ])),
            // Campo de contraseña con validadores de requerido y longitud mínima de 6 caracteres
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required])),
        });
    }
    /**
     * Método que realiza el login apoyándose en el servicio Auth y redirecciona al home.
     * Este método usa el servicio de AuthService para iniciar sesión con el email y la contraseña introducidos en el formulario.
     * Si el login es exitoso, se usa el servicio de NavController para navegar a la página de home.
     * También se usa el servicio de UserService para guardar el email del usuario logado.
     * Si el login es erróneo, se muestra un mensaje de error en la consola.
     * @param {any} value Los valores pasados por el formulario de login
     * @returns {void}
     */
    logIn(value) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            // Usar un bloque try-catch para manejar posibles errores
            try {
                // Iniciar sesión con el email y la contraseña usando el servicio de AuthService
                yield this.authSvc.signIn(value);
                // Navegar a la página de home usando el servicio de NavController
                this.navCtrl.navigateRoot('/home');
                // Obtener el email del usuario logado del formulario de login
                const userEmail = this.loginForm.value.email;
                // Guardar el email del usuario logado usando el servicio de UserService
                this.userSvc.setUserEmail(userEmail);
                // Mostrar un mensaje de éxito en la consola con el email del usuario logado
                console.log('logado con', userEmail);
            }
            catch (error) {
                // Mostrar un mensaje de error en la consola con el error ocurrido
                console.log('Error al iniciar sesión:', error);
            }
        });
    }
    /**
     * Método para la autenticación de Google (NO IMPLEMENTADO).
     * Este método está vacío porque no se ha implementado la funcionalidad de iniciar sesión con Google.
     * @param {void}
     * @returns {void}
     */
    gAuth() { }
    // No hacer nada
    /**
     * Método que muestra un modal para el registro del usuario.
     * Este método usa el controlador de Modal de Ionic para crear y presentar una ventana modal con el componente RegistroComponent, que permite al usuario crear una cuenta con Firebase Authentication.
     * @param {void}
     * @returns {Promise<void>} Una promesa que se resuelve cuando el modal se presenta
     */
    registroModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            // Crear el modal con el componente RegistroComponent y una clase CSS personalizada usando el controlador de Modal de Ionic
            const modal = yield this.modalCtrl.create({
                component: _registro_registro_component__WEBPACK_IMPORTED_MODULE_3__.RegistroComponent,
                cssClass: 'loginRegisterModal',
            });
            // Presentar el modal y devolver la promesa que se resuelve cuando el modal se presenta
            return yield modal.present();
        });
    }
    /**
     * Método que cierra el propio modal.
     * Este método usa el controlador de Modal de Ionic para cerrar el modal que muestra el formulario de login.
     * @param {void}
     * @returns {void}
     */
    closeModal() {
        // Cerrar el modal usando el controlador de Modal de Ionic
        this.modalCtrl.dismiss();
    }
};
LoginComponent.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__.UserService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.NavController }
];
LoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-login',
        template: _login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], LoginComponent);



/***/ }),

/***/ 5819:
/*!***************************************************!*\
  !*** ./src/app/components/menu/menu.component.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MenuComponent": () => (/* binding */ MenuComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _menu_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu.component.html?ngResource */ 2574);
/* harmony import */ var _menu_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.component.scss?ngResource */ 1346);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 2816);





let MenuComponent = class MenuComponent {
    /**
     * Constructor de la clase
     * @param router Servicio de Angular que permite navegar entre las páginas de la aplicación mediante las urls
     */
    constructor(router) {
        this.router = router;
    }
    /**
     * Método de inicio que se ejecuta cuando el componente se inicializa.
     * No hace nada en este caso.
     */
    ngOnInit() { }
    /**
     * Método que navega a la página solicitada.
     * Este método usa el servicio de Router para navegar a la url pasada por parámetro, que corresponde a una de las opciones de menú.
     * @param {string} url La url a la que va a navegar
     * @returns {void}
     */
    navigateToPage(url) {
        // Navegar a la url usando el servicio de Router
        this.router.navigate([url]);
    }
};
MenuComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router }
];
MenuComponent.propDecorators = {
    menuOpts: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
MenuComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-menu',
        template: _menu_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_menu_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], MenuComponent);



/***/ }),

/***/ 6356:
/*!***********************************************************!*\
  !*** ./src/app/components/new-reto/new-reto.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewRetoComponent": () => (/* binding */ NewRetoComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _new_reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./new-reto.component.html?ngResource */ 1540);
/* harmony import */ var _new_reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./new-reto.component.scss?ngResource */ 4758);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ 7556);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/camera */ 4241);
/* harmony import */ var _services_multimedia_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/multimedia.service */ 4579);
/* harmony import */ var _services_reto_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/reto.service */ 1268);











let NewRetoComponent = class NewRetoComponent {
    /**
     *
     * @param modalCtrl Controlador del Modal
     * @param fBuilder Constructor del Formulario
     * @param authSvc Servicio para gestionar los inicios de sesión y su estado
     * @param sanitizer Saneador para la imagen, prepara una URL segura
     * @param multimediaSvc Servicio de subida de imagen a Firestorage
     * @param retoSvc Servicio de manejo de Retos
     * @param alertCtrl Controlador de Alertas
     */
    constructor(modalCtrl, fBuilder, authSvc, sanitizer, multimediaSvc, retoSvc, alertCtrl) {
        this.modalCtrl = modalCtrl;
        this.fBuilder = fBuilder;
        this.authSvc = authSvc;
        this.sanitizer = sanitizer;
        this.multimediaSvc = multimediaSvc;
        this.retoSvc = retoSvc;
        this.alertCtrl = alertCtrl;
    }
    /**
     * En el inicio cargamos el usuario logado.
     * Tambien construimos el formulario con sus validadores
     */
    ngOnInit() {
        this.authSvc.getUserEmail().then(email => {
            this.userEmail = email;
        });
        this.newRetoForm = this.fBuilder.group({
            TITULO: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required
            ])),
            DESCRIPCION: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required
            ])),
            TIPO: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required
            ])),
            NIVEL: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required
            ])),
            ACTIVO: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(true, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required
            ])),
        });
    }
    /**
     * Metodo que selecciona una imagen de la galeria y la sanea para luego subirla a Firestorage a través del Servicio
     */
    selectImage() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const foto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.Camera.getPhoto({
                quality: 90,
                allowEditing: false,
                resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraResultType.DataUrl,
                source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraSource.Photos,
            });
            //Hay que sanearla
            this.image = this.sanitizer.bypassSecurityTrustResourceUrl(foto && (foto.dataUrl));
            let blob = yield fetch(foto.dataUrl).then(r => r.blob());
            this.imagenSaneada = blob;
            console.log('tenemos imagen', blob);
        });
    }
    /**
     * Metodo que recoge los valores del formulario, los asigna al objeto Reto y lo crea en Firestore a través del Servicio de Retos
     * @param value Valores recogidos por el Formulario
     */
    crearReto(value) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            console.log('vamos a crear el reto');
            try {
                const res = yield this.multimediaSvc.subirImagen(this.imagenSaneada, 'retos', this.userEmail);
                this.reto = value;
                this.reto.DESTACADO = true;
                this.reto.RETADOR = this.userEmail;
                this.reto.IMAGEN = res;
                console.log('Arriba el reto con', this.reto.TITULO, this.reto.DESCRIPCION);
                this.retoSvc.addReto(this.reto);
            }
            catch (error) {
                console.log('Error creando el Reto:', error);
            }
        });
    }
    /**
     * Alerta para pedir confirmación
     */
    presentAlert() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            console.log(this.newRetoForm.value);
        });
    }
    /**
     * Cerramos el modal
     */
    closeModal() {
        this.modalCtrl.dismiss();
    }
};
NewRetoComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ModalController },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__.DomSanitizer },
    { type: _services_multimedia_service__WEBPACK_IMPORTED_MODULE_4__.MultimediaService },
    { type: _services_reto_service__WEBPACK_IMPORTED_MODULE_5__.RetoService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController }
];
NewRetoComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-new-reto',
        template: _new_reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_new_reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], NewRetoComponent);



/***/ }),

/***/ 4217:
/*!*********************************************************!*\
  !*** ./src/app/components/noticia/noticia.component.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NoticiaComponent": () => (/* binding */ NoticiaComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _noticia_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./noticia.component.html?ngResource */ 4248);
/* harmony import */ var _noticia_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./noticia.component.scss?ngResource */ 6341);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 7122);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 6436);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 3819);







let NoticiaComponent = class NoticiaComponent {
    /**
     *
     * @param iab Componente de InAppBrowser para mostrar las noticias sin salir de la aplicación
     * @param platform Componente para controlar la plataforma dónde se ejecuta la aplicación. si es Android, se usará el IAB
     * @param actionSheetCtrl Componente personalizado para mostrar la opcion de compartir
     * @param socialSharing Componente para compartir la noticia a través de las aplicaciones instaladas
     */
    constructor(iab, platform, actionSheetCtrl, socialSharing) {
        this.iab = iab;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.socialSharing = socialSharing;
    }
    /**
     * Método que muestra el ActinioSheet con la opcion de compartir la noticia
     */
    onOpenMenu() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Opciones',
                buttons: [{
                        text: 'Compartir',
                        icon: 'share-outline',
                        cssClass: 'primary',
                        handler: () => {
                            this.onShareArticle();
                        }
                    }, {
                        text: 'Cancelar',
                        icon: 'close-outline',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }]
            });
            yield actionSheet.present();
        });
    }
    /**
     * Abre el articulo en el navegador. Si es Android lo muestra en el navegador dentro de la App, si es en Web, lo muestra en otra pestaña
     * @returns
     */
    openArticle() {
        if (this.platform.is('android')) {
            const browser = this.iab.create(this.article.url);
            browser.show();
            return;
        }
    }
    /**
     * Método para compartir la noticia a través de las aplicaciones instalas en el dispositivo
     */
    onShareArticle() {
        if (this.platform.is('cordova')) {
            this.socialSharing.share(this.article.title, this.article.url);
        }
        else {
            if (navigator.share) {
                navigator.share({
                    title: this.article.title,
                    text: this.article.description,
                    url: this.article.url,
                })
                    .then(() => console.log('Compartido!'))
                    .catch((error) => console.log('Error compartiendo', error));
            }
        }
    }
};
NoticiaComponent.ctorParameters = () => [
    { type: _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__.InAppBrowser },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ActionSheetController },
    { type: _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__.SocialSharing }
];
NoticiaComponent.propDecorators = {
    article: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }],
    index: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }]
};
NoticiaComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-noticia',
        template: _noticia_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_noticia_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], NoticiaComponent);



/***/ }),

/***/ 4101:
/*!***********************************************************!*\
  !*** ./src/app/components/noticias/noticias.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NoticiasComponent": () => (/* binding */ NoticiasComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _noticias_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./noticias.component.html?ngResource */ 1412);
/* harmony import */ var _noticias_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./noticias.component.scss?ngResource */ 7622);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);




let NoticiasComponent = class NoticiasComponent {
    /**
     * Constructor de Clase. Este Componente solo se encarga de cargar el Componente Artículo, para adaptarlo según dispositivo.
     */
    constructor() {
        /**
         * Variable tipo Articulo para la carga de Articulos. Publica
         */
        this.article = [];
        this.articles = [];
    }
};
NoticiasComponent.ctorParameters = () => [];
NoticiasComponent.propDecorators = {
    articles: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
NoticiasComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-noticias',
        template: _noticias_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_noticias_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], NoticiasComponent);



/***/ }),

/***/ 43:
/*!***********************************************************!*\
  !*** ./src/app/components/registro/registro.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistroComponent": () => (/* binding */ RegistroComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _registro_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registro.component.html?ngResource */ 1342);
/* harmony import */ var _registro_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registro.component.scss?ngResource */ 1483);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);







let RegistroComponent = class RegistroComponent {
    /**
     * Constructor de clase
     * @param authSrv Servicio de Autenticación personalizado
     * @param fBuilder Componente de manejo de Formulario
     * @param modalCtrl Componente de manejo del Modal
     */
    constructor(authSrv, fBuilder, modalCtrl) {
        this.authSrv = authSrv;
        this.fBuilder = fBuilder;
        this.modalCtrl = modalCtrl;
        /**
         * Mensajes de proceso correcto
         */
        this.successMsg = '';
        /**
         * Mensajes de Error declarados en Interfaces
         */
        this.errorMsg = [];
    }
    /**
     * Construimos el Formulario de registro asignando los valores y validando patrones de email y contraseña.
     * También marcamos los campos obligatorios.
     */
    ngOnInit() {
        this.registroForm = this.fBuilder.group({
            NOMBRE: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required
            ])),
            EMAIL: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            PASSWORD: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.minLength(6),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required
            ])),
        });
    }
    /**
     * Este método envia al Servicio de Autenticacion los valores del Formulario y muestra mensaje según resultado.
     * @param value Valor recibido a través del formulario
     */
    registro(value) {
        this.authSrv.signUp(value)
            .then((response) => {
            this.errorMsg = [];
            this.successMsg = "Usuario Creado.";
            this.modalCtrl.dismiss();
        }, error => {
            this.errorMsg = error.message;
            this.successMsg = "";
        });
    }
};
RegistroComponent.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ModalController }
];
RegistroComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-registro',
        template: _registro_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_registro_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RegistroComponent);



/***/ }),

/***/ 9860:
/*!***************************************************!*\
  !*** ./src/app/components/reto/reto.component.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RetoComponent": () => (/* binding */ RetoComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reto.component.html?ngResource */ 6585);
/* harmony import */ var _reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reto.component.scss?ngResource */ 7266);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 6436);
/* harmony import */ var src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/reto.service */ 1268);
/* harmony import */ var _info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../info-reto/info-reto.component */ 8083);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/avisos.service */ 9595);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/user.service */ 3071);











let RetoComponent = class RetoComponent {
    /**
     * constructor de clase
     * @param retoSvc Servicio para el manejo de los retos
     * @param modalCtrl Controlador de uso de modales
     * @param socialSharing Componente usado para compartir retos en aplicaciones
     * @param platform Componente para comprobar la plataforma de ejecucion
     * @param userSvc Servicio de manejo del usuario
     * @param authSvc Servicio para controlar la autenticacion
     * @param avisosSvc Servicio de avisos a traves dded toast
     */
    constructor(retoSvc, modalCtrl, socialSharing, platform, userSvc, authSvc, avisosSvc) {
        /*  this.userSvc.getUserEmail().subscribe(email => {
           this.userEmail = email;
           console.log('email', this.userEmail);
         }); */
        this.retoSvc = retoSvc;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.platform = platform;
        this.userSvc = userSvc;
        this.authSvc = authSvc;
        this.avisosSvc = avisosSvc;
        /**
         * Comprueba si el usuario está logado o no para habilitar el icono de favoritos y de Realizado.
         */
        this.usuarioLogado = false;
        /**
         * Declaramos Array de Retos para guardar la info para mostrar.
         */
        this.retos = [];
        /**
          * Declaramos Array de Favoritos para guardar la info para mostrar.
          */
        this.favoritos = [];
        /**
         * Mensaje para compartir por RRSS.
         */
        this.mensaje = 'Te reto!';
        this.authSvc.initAuthStateListener();
        this.userEmail = this.authSvc.userEmail;
    }
    /**
     * Método de inicio
     * Checkeamos si el usuario logado tiene el reto en favorito o conseguido para mostrar los iconos correspondientes
     */
    ngOnInit() {
        console.log(this.reto.ID, this.userEmail);
        this.checkRetoFavorito(this.reto.ID, this.userEmail);
        this.checkRetoConseguido(this.reto.ID, this.userEmail);
    }
    /**
     * Carga un modal para mostar la info detallada de un reto
     * @param retoId Id del Reto a mostrar
     */
    verDetalle(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _info_reto_info_reto_component__WEBPACK_IMPORTED_MODULE_4__.InfoRetoComponent,
                componentProps: {
                    retoId
                },
                cssClass: 'modalInfo'
            });
            modal.present();
        });
    }
    /**
     * CAmbia el Icono del reto según el tipo (ocular, telescopio, prismaticos)
     * @param tipo Tipo de Reto
     * @returns el nombre del icono de ionicons.io
     */
    getIconTipo(tipo) {
        switch (tipo) {
            case 'telescopio':
                return 'telescope-outline';
            case 'prismaticos':
                return 'recording-outline';
            case 'ocular':
                return 'eye-outline';
            default:
                return 'help';
        }
    }
    /**
     * Cambia el color del icono segun el nivel de dificultad del Reto
     * @param nivel Nivel del Reto
     * @returns El color del icono (verde, amarillo, rojo)
     */
    getColorNivel(nivel) {
        switch (nivel) {
            case 'facil':
                return 'success';
            case 'intermedio':
                return 'warning';
            case 'dificil':
                return 'danger';
            default:
                return 'help';
        }
    }
    /**
     * Método para compartir el reto en las aplicaciones disponibles en el terminal (Mail o RRSS)
     */
    compartirReto() {
        if (this.platform.is('cordova')) {
            this.socialSharing.share(this.mensaje);
        }
        else {
            if (navigator.share) {
                navigator.share({
                    text: this.mensaje,
                    url: ''
                })
                    .then(() => console.log('Compartido!'))
                    .catch((error) => console.log('Error compartiendo', error));
            }
        }
    }
    /**
     * Gestiona si el reto esta en Favorito o no para mostrar el icono correspondiente
     */
    checkRetoFavorito(retoId, user) {
        this.retoSvc.checkFavorito(retoId, user).subscribe(existe => {
            this.esFavorito = existe;
            console.log(this.esFavorito);
        });
    }
    /**
     * Metodo para marcar un reto como favorito
     * @param retoId Id del reto a marcar
     * @param user Email del usuario logado
     */
    setFavorito(retoId, user) {
        try {
            if (user) {
                this.retoSvc.addFavorito(retoId, user);
                this.avisosSvc.presentToast('Favorito añadido', 'success');
            }
            else {
                this.avisosSvc.presentToast('Debes estar logado para hacer esto', 'warning');
            }
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al añadir Favorito', 'danger');
        }
    }
    /**
   * Metodo para desmarcar un reto como favorito
   * @param retoId Id del reto a marcar
   * @param user Email del usuario logado
   */
    quitarFavorito(retoId, email) {
        try {
            this.retoSvc.getFavorito(retoId, email).subscribe(fav => {
                this.retoSvc.deleteFavorito(fav[0].ID_FAV);
            });
            this.avisosSvc.presentToast('Favorito eliminado correctamente', 'success');
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar el Favorito', 'danger');
        }
    }
    /**
     * Gestiona si el reto esta esta conseguido por el usuario o no para mostrar el icono correspondiente
     */
    checkRetoConseguido(retoId, user) {
        this.retoSvc.checkRetoConseguido(retoId, user).subscribe(existe => {
            this.esRetoConseguido = existe;
            console.log(this.esRetoConseguido);
        });
    }
    /**
   * Metodo para marcar un reto como conseguido
   * @param retoId Id del reto a marcar
   * @param user Email del usuario logado
   */
    setRetoConseguido(retoId, user) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            try {
                if (user) {
                    yield this.retoSvc.addRetoConseguido(retoId, user);
                    this.avisosSvc.presentToast('Reto conseguido', 'success');
                    /*  this.userSvc.getTotalPuntosByUser(this.userEmail).subscribe(totalPuntos => {
             
                      this.userSvc.updateUserPuntos(this.userEmail, totalPuntos);
                     }); */
                }
                else {
                    this.avisosSvc.presentToast('Debes estar logado para hacer esto', 'warning');
                }
            }
            catch (error) {
                this.avisosSvc.presentToast('Error al conseguir Reto', 'danger');
            }
        });
    }
    /**
   * Metodo para desmarcar el reto conseguido
   * @param retoId Id del reto a marcar
   * @param user Email del usuario logado
   */
    removeRetoConseguido(retoId, email) {
        try {
            this.retoSvc.getRetoConseguido(retoId, email).subscribe(retoConseguido => {
                this.retoSvc.deleteRetoConseguido(retoConseguido[0].ID_RETO_CONSEGUIDO);
                this.avisosSvc.presentToast('Reto Conseguido eliminado', 'success');
            });
        }
        catch (error) {
            this.avisosSvc.presentToast('Error al eliminar Reto', 'danger');
        }
    }
};
RetoComponent.ctorParameters = () => [
    { type: src_app_services_reto_service__WEBPACK_IMPORTED_MODULE_3__.RetoService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ModalController },
    { type: _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_2__.SocialSharing },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_7__.UserService },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__.AuthService },
    { type: src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_6__.AvisosService }
];
RetoComponent.propDecorators = {
    reto: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.Input }],
    index: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.Input }]
};
RetoComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-reto',
        template: _reto_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_reto_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RetoComponent);



/***/ }),

/***/ 1255:
/*!*****************************************************!*\
  !*** ./src/app/components/retos/retos.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RetosComponent": () => (/* binding */ RetosComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _retos_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./retos.component.html?ngResource */ 6894);
/* harmony import */ var _retos_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./retos.component.scss?ngResource */ 6053);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);




let RetosComponent = class RetosComponent {
    /**
     * Constructor de clase
     */
    constructor() {
        /**
         * Array de Retos
         */
        this.reto = [];
        this.retos = [];
    }
};
RetosComponent.ctorParameters = () => [];
RetosComponent.propDecorators = {
    retos: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
RetosComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-retos',
        template: _retos_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_retos_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RetosComponent);



/***/ }),

/***/ 4579:
/*!************************************************!*\
  !*** ./src/app/services/multimedia.service.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MultimediaService": () => (/* binding */ MultimediaService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/compat/storage */ 5574);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ 4661);




let MultimediaService = class MultimediaService {
    /**
     * Constructor de clase
     * @param fireStorage Servidcio de almacenamiento de archivos de Firebase
     */
    constructor(fireStorage) {
        this.fireStorage = fireStorage;
    }
    /**
     * Subida de la imagen a una ruta pasada por parametro
     * @param file archivo saneado que tiene que subir a Firestorage
     * @param path ruta en la que va a alojar el archivo
     * @param nombre nombre del archivo
     * @returns
     */
    subirImagen(file, path, nombre) {
        return new Promise(resolve => {
            const filePath = path + '/' + nombre + Date.now();
            const ref = this.fireStorage.ref(filePath);
            const task = ref.put(file);
            task.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_0__.finalize)(() => {
                ref.getDownloadURL().subscribe(res => {
                    const downloadURL = res;
                    resolve(downloadURL);
                    return;
                });
            }))
                .subscribe();
        });
    }
};
MultimediaService.ctorParameters = () => [
    { type: _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_1__.AngularFireStorage }
];
MultimediaService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
    })
], MultimediaService);



/***/ }),

/***/ 1268:
/*!******************************************!*\
  !*** ./src/app/services/reto.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RetoService": () => (/* binding */ RetoService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 2393);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 6942);
/* harmony import */ var _avisos_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./avisos.service */ 9595);





let RetoService = class RetoService {
    /**
     * Constructor de clase
     * @param firestore Servicio de Angular firestore
     * @param avisosSvc Servicio de avisos via toast
     */
    constructor(firestore, avisosSvc) {
        this.firestore = firestore;
        this.avisosSvc = avisosSvc;
    }
    /**
     * Obtiene un Observable con todos los retos creados
     * @returns
     */
    getRetos() {
        return this.firestore.collection('retos').valueChanges({ idField: 'id' });
    }
    /**
     * Obtiene un array con los retos Activos, que son los que se muestran en el Home
     * @returns
     */
    getRetosActivos() {
        this.retosCollection = this.firestore.collection('retos', ref => ref.where('ACTIVO', '==', true));
        this.retos = this.retosCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return Object.assign({ id }, data);
        })));
        return this.retos;
    }
    /**
     * Obtiene un Observable tipo Reto de un reto pasado por parámetro
     * @param retoId Id del reto
     * @returns
     */
    getRetosById(retoId) {
        this.retosCollection = this.firestore.collection('retos', ref => ref.where('ID', '==', retoId));
        this.retos = this.retosCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const $id = a.payload.doc.id;
            return Object.assign({ $id }, data);
        })));
        return this.retos;
    }
    /**
     * Obtiene un array de Retos creados por un usuario pasado por parámetro
     * @param userId Id del usuario
     * @returns
     */
    getRetosByUser(userId) {
        this.retosCollection = this.firestore.collection('retos', ref => ref.where('RETADOR', '==', userId));
        this.retos = this.retosCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const $id = a.payload.doc.id;
            return Object.assign({ $id }, data);
        })));
        return this.retos;
    }
    /**
     * Actualiza el estado del reto: Activo <=> Inactivo
     * @param retoId Id del reto a Actualizar
     * @returns
     */
    updateEstadoReto(retoId) {
        this.getRetosById(retoId).subscribe(reto => {
            this.reto = reto[0];
        });
        if (this.reto.ACTIVO == true) {
            this.reto.ACTIVO = false;
        }
        else {
            this.reto.ACTIVO = true;
        }
        return this.firestore.collection('retos').doc(retoId).update({
            ACTIVO: this.reto.ACTIVO
        });
    }
    /**
     * Método que comprueba si un reto está Activo o no
     * @param retoId Id del reto a revisar el estado
     * @returns
     */
    checkRetoActivo(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            console.log('check', retoId);
            const doc = yield this.firestore.collection('retos').doc(retoId).get().toPromise();
            return doc.exists && doc.data().ACTIVO;
        });
    }
    /**
     * Método para crear un nuevo reto
     * @param reto Valores pasados desde el formulario
     */
    addReto(reto) {
        this.firestore.collection('retos').add({
            TITULO: reto.TITULO,
            DESCRIPCION: reto.DESCRIPCION,
            TIPO: reto.TIPO,
            NIVEL: reto.NIVEL,
            ACTIVO: reto.ACTIVO,
            DESTACADO: reto.DESTACADO,
            RETADOR: reto.RETADOR,
            IMAGEN: reto.IMAGEN,
            ID: ""
        })
            .then((docRef) => {
            this.firestore.doc(docRef).update({
                ID: docRef.id
            });
        })
            .catch((error) => {
            console.error('Error al crear el reto: ', error);
            this.avisosSvc.presentToast('No se ha podido crear el Reto', 'danger');
        });
    }
    /**
     * Método para eliminar un reto concreto
     * @param retoId Id del reto a eliminar
     */
    deleteReto(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            this.firestore.collection('retos').doc(retoId).delete()
                .then(() => {
                this.avisosSvc.presentToast('Reto eliminado correctamente', 'success');
            })
                .catch((error) => {
                this.avisosSvc.presentToast('No se ha podido eliminar el Reto', 'danger');
                console.error('Error al eliminar Reto: ', error);
            });
        });
    }
    /**
     * Obtiene un Observable de los favoritos
     * @returns
     */
    getFavoritos() {
        return this.firestore.collection('favoritos').valueChanges({ idField: 'id' });
    }
    /**
     * Obtiene una lista de favoritos de un usuario concreto
     * @param userId Id del usuario para filtrar los favoritos
     * @returns
     */
    getFavoritosByUser(userId) {
        this.favCollection = this.firestore.collection('favoritos', ref => ref.where('USER', '==', userId));
        this.retosFavoritos = this.favCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const $id = a.payload.doc.id;
            return Object.assign({ $id }, data);
        })));
        return this.retosFavoritos;
    }
    /**
     * Comprueba si un usuario tiene un reto como Favorito
     * @param retoId Id del reto a comprobar
     * @param user Email del usuario a comprobar
     * @returns
     */
    checkFavorito(retoId, user) {
        return this.firestore.collection('favoritos', ref => ref.where('ID_RETO', '==', retoId).where('USER', '==', user)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(favoritos => favoritos.length > 0));
    }
    /**
     * Obtiene los datos de un reto favorito concreto
     * @param retoId Reto a cargar
     * @param user email del usuario
     * @returns
     */
    getFavorito(retoId, user) {
        this.favCollection = this.firestore.collection('favoritos', ref => ref.where('ID_RETO', '==', retoId).where('USER', '==', user));
        this.retosFavoritos = this.favCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return Object.assign({ id }, data);
        })));
        return this.retosFavoritos;
    }
    /**
     * Añade un favorito al usaurio actual
     * @param retoId Id del reto
     * @param user email del usuario
     */
    addFavorito(retoId, user) {
        this.firestore.collection('favoritos').add({
            USER: user,
            ID_RETO: retoId,
            ID_FAV: ""
        })
            .then((docRef) => {
            this.firestore.doc(docRef).update({
                ID_FAV: docRef.id
            });
        })
            .catch((error) => {
            console.error('Error al agregar el Favorito: ', error);
        });
    }
    /**
     * Elimina un favorito de la lista
     * @param favId id del favorito
     */
    deleteFavorito(favId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            this.firestore.collection('favoritos').doc(favId).delete()
                .then(() => {
                console.log('Favorito eliminado correctamente');
            })
                .catch((error) => {
                console.error('Error al eliminar Favorito: ', error);
            });
        });
    }
    /**
     * Obtiene el nivel de dificultad de un reto concreto
     * @param retoId Id del reto
     * @returns
     */
    getNivel(retoId) {
        return this.getRetosById(retoId).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(reto => reto[0].NIVEL));
    }
    /**
     * Revisa si el reto ha sido conseguido por un usuario
     * @param retoId Id del reto
     * @param user email del usuario
     * @returns
     */
    checkRetoConseguido(retoId, user) {
        console.log('buscando reto conseguido', retoId, 'del usuario', user);
        return this.firestore.collection('retosconseguidos', ref => ref.where('ID_RETO', '==', retoId).where('USER', '==', user)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(favoritos => favoritos.length > 0));
    }
    /**
     * Obtiene un listado de todos los retos conseguidos
     * @returns
     */
    getRetosConseguidos() {
        return this.firestore.collection('retosconseguidos').valueChanges({ idField: 'id' });
    }
    /**
     * Obtiene una lista de retos conseguidos por un usuario concreto
     * @param userId Id del Usuario
     * @returns
     */
    getRetosConseguidosByUser(userId) {
        this.retosConseguidosCollection = this.firestore.collection('retosconseguidos', ref => ref.where('USER', '==', userId));
        this.retosConseguidos = this.retosConseguidosCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const $id = a.payload.doc.id;
            return Object.assign({ $id }, data);
        })));
        return this.retosConseguidos;
    }
    /**
     * Obtiene la informacion del reto conseguido
     * @param retoId Id del Reto
     * @param user Email del usuario
     * @returns
     */
    getRetoConseguido(retoId, user) {
        this.retosConseguidosCollection = this.firestore.collection('retosconseguidos', ref => ref.where('ID_RETO', '==', retoId).where('USER', '==', user));
        this.retosConseguidos = this.retosConseguidosCollection.snapshotChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(actions => actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return Object.assign({ id }, data);
        })));
        return this.retosConseguidos;
    }
    /**
     * Crea una nueva entrada de reto conseguido.
     * Dependiendo del nivel, obtiene los puntos para añadirlos a la tabla
     * @param retoId id del reto conseguido
     * @param user usuario que consigue el reto
     */
    addRetoConseguido(retoId, user) {
        this.getNivel(retoId).subscribe(nivel => {
            this.nivel = nivel;
            switch (this.nivel) {
                case 'facil':
                    this.puntos = 1;
                    break;
                case 'intermedio':
                    this.puntos = 3;
                    break;
                case 'dificil':
                    this.puntos = 5;
                    break;
            }
            ;
            this.firestore.collection('retosconseguidos').add({
                USER: user,
                ID_RETO: retoId,
                ID_RETO_CONSEGUIDO: '',
                PUNTOS: this.puntos
            }).then((docRef) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
                yield this.firestore.doc(docRef).update({
                    ID_RETO_CONSEGUIDO: docRef.id
                });
            }))
                .catch((error) => {
                console.error('Error al agregar el reto conseguido: ', error);
            });
        });
    }
    /**
     * Elimina un reto conseguido
     * @param retoId id del reto conseguido
     */
    deleteRetoConseguido(retoId) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            this.firestore.collection('retosconseguidos').doc(retoId).delete().then(() => {
                console.log("Documento eliminado correctamente");
            }).catch((error) => {
                console.error("Error al eliminar el documento: ", error);
            });
        });
    }
};
RetoService.ctorParameters = () => [
    { type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__.AngularFirestore },
    { type: _avisos_service__WEBPACK_IMPORTED_MODULE_0__.AvisosService }
];
RetoService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], RetoService);



/***/ }),

/***/ 4830:
/*!****************************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/definitions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraDirection": () => (/* binding */ CameraDirection),
/* harmony export */   "CameraResultType": () => (/* binding */ CameraResultType),
/* harmony export */   "CameraSource": () => (/* binding */ CameraSource)
/* harmony export */ });
var CameraSource;

(function (CameraSource) {
  /**
   * Prompts the user to select either the photo album or take a photo.
   */
  CameraSource["Prompt"] = "PROMPT";
  /**
   * Take a new photo using the camera.
   */

  CameraSource["Camera"] = "CAMERA";
  /**
   * Pick an existing photo from the gallery or photo album.
   */

  CameraSource["Photos"] = "PHOTOS";
})(CameraSource || (CameraSource = {}));

var CameraDirection;

(function (CameraDirection) {
  CameraDirection["Rear"] = "REAR";
  CameraDirection["Front"] = "FRONT";
})(CameraDirection || (CameraDirection = {}));

var CameraResultType;

(function (CameraResultType) {
  CameraResultType["Uri"] = "uri";
  CameraResultType["Base64"] = "base64";
  CameraResultType["DataUrl"] = "dataUrl";
})(CameraResultType || (CameraResultType = {}));

/***/ }),

/***/ 4241:
/*!**********************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Camera": () => (/* binding */ Camera),
/* harmony export */   "CameraDirection": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraDirection),
/* harmony export */   "CameraResultType": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraResultType),
/* harmony export */   "CameraSource": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraSource)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 5099);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 4830);

const Camera = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Camera', {
  web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_camera_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 1327)).then(m => new m.CameraWeb())
});



/***/ }),

/***/ 5099:
/*!****************************************************!*\
  !*** ./node_modules/@capacitor/core/dist/index.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Capacitor": () => (/* binding */ Capacitor),
/* harmony export */   "CapacitorCookies": () => (/* binding */ CapacitorCookies),
/* harmony export */   "CapacitorException": () => (/* binding */ CapacitorException),
/* harmony export */   "CapacitorHttp": () => (/* binding */ CapacitorHttp),
/* harmony export */   "CapacitorPlatforms": () => (/* binding */ CapacitorPlatforms),
/* harmony export */   "ExceptionCode": () => (/* binding */ ExceptionCode),
/* harmony export */   "Plugins": () => (/* binding */ Plugins),
/* harmony export */   "WebPlugin": () => (/* binding */ WebPlugin),
/* harmony export */   "WebView": () => (/* binding */ WebView),
/* harmony export */   "addPlatform": () => (/* binding */ addPlatform),
/* harmony export */   "registerPlugin": () => (/* binding */ registerPlugin),
/* harmony export */   "registerWebPlugin": () => (/* binding */ registerWebPlugin),
/* harmony export */   "setPlatform": () => (/* binding */ setPlatform)
/* harmony export */ });
/* harmony import */ var c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 1670);


/*! Capacitor: https://capacitorjs.com/ - MIT License */
const createCapacitorPlatforms = win => {
  const defaultPlatformMap = new Map();
  defaultPlatformMap.set('web', {
    name: 'web'
  });
  const capPlatforms = win.CapacitorPlatforms || {
    currentPlatform: {
      name: 'web'
    },
    platforms: defaultPlatformMap
  };

  const addPlatform = (name, platform) => {
    capPlatforms.platforms.set(name, platform);
  };

  const setPlatform = name => {
    if (capPlatforms.platforms.has(name)) {
      capPlatforms.currentPlatform = capPlatforms.platforms.get(name);
    }
  };

  capPlatforms.addPlatform = addPlatform;
  capPlatforms.setPlatform = setPlatform;
  return capPlatforms;
};

const initPlatforms = win => win.CapacitorPlatforms = createCapacitorPlatforms(win);
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */


const CapacitorPlatforms = /*#__PURE__*/initPlatforms(typeof globalThis !== 'undefined' ? globalThis : typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : {});
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */

const addPlatform = CapacitorPlatforms.addPlatform;
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */

const setPlatform = CapacitorPlatforms.setPlatform;

const legacyRegisterWebPlugin = (cap, webPlugin) => {
  var _a;

  const config = webPlugin.config;
  const Plugins = cap.Plugins;

  if (!config || !config.name) {
    // TODO: add link to upgrade guide
    throw new Error(`Capacitor WebPlugin is using the deprecated "registerWebPlugin()" function, but without the config. Please use "registerPlugin()" instead to register this web plugin."`);
  } // TODO: add link to upgrade guide


  console.warn(`Capacitor plugin "${config.name}" is using the deprecated "registerWebPlugin()" function`);

  if (!Plugins[config.name] || ((_a = config === null || config === void 0 ? void 0 : config.platforms) === null || _a === void 0 ? void 0 : _a.includes(cap.getPlatform()))) {
    // Add the web plugin into the plugins registry if there already isn't
    // an existing one. If it doesn't already exist, that means
    // there's no existing native implementation for it.
    // - OR -
    // If we already have a plugin registered (meaning it was defined in the native layer),
    // then we should only overwrite it if the corresponding web plugin activates on
    // a certain platform. For example: Geolocation uses the WebPlugin on Android but not iOS
    Plugins[config.name] = webPlugin;
  }
};

var ExceptionCode;

(function (ExceptionCode) {
  /**
   * API is not implemented.
   *
   * This usually means the API can't be used because it is not implemented for
   * the current platform.
   */
  ExceptionCode["Unimplemented"] = "UNIMPLEMENTED";
  /**
   * API is not available.
   *
   * This means the API can't be used right now because:
   *   - it is currently missing a prerequisite, such as network connectivity
   *   - it requires a particular platform or browser version
   */

  ExceptionCode["Unavailable"] = "UNAVAILABLE";
})(ExceptionCode || (ExceptionCode = {}));

class CapacitorException extends Error {
  constructor(message, code, data) {
    super(message);
    this.message = message;
    this.code = code;
    this.data = data;
  }

}

const getPlatformId = win => {
  var _a, _b;

  if (win === null || win === void 0 ? void 0 : win.androidBridge) {
    return 'android';
  } else if ((_b = (_a = win === null || win === void 0 ? void 0 : win.webkit) === null || _a === void 0 ? void 0 : _a.messageHandlers) === null || _b === void 0 ? void 0 : _b.bridge) {
    return 'ios';
  } else {
    return 'web';
  }
};

const createCapacitor = win => {
  var _a, _b, _c, _d, _e;

  const capCustomPlatform = win.CapacitorCustomPlatform || null;
  const cap = win.Capacitor || {};
  const Plugins = cap.Plugins = cap.Plugins || {};
  /**
   * @deprecated Use `capCustomPlatform` instead, default functions like registerPlugin will function with the new object.
   */

  const capPlatforms = win.CapacitorPlatforms;

  const defaultGetPlatform = () => {
    return capCustomPlatform !== null ? capCustomPlatform.name : getPlatformId(win);
  };

  const getPlatform = ((_a = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _a === void 0 ? void 0 : _a.getPlatform) || defaultGetPlatform;

  const defaultIsNativePlatform = () => getPlatform() !== 'web';

  const isNativePlatform = ((_b = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _b === void 0 ? void 0 : _b.isNativePlatform) || defaultIsNativePlatform;

  const defaultIsPluginAvailable = pluginName => {
    const plugin = registeredPlugins.get(pluginName);

    if (plugin === null || plugin === void 0 ? void 0 : plugin.platforms.has(getPlatform())) {
      // JS implementation available for the current platform.
      return true;
    }

    if (getPluginHeader(pluginName)) {
      // Native implementation available.
      return true;
    }

    return false;
  };

  const isPluginAvailable = ((_c = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _c === void 0 ? void 0 : _c.isPluginAvailable) || defaultIsPluginAvailable;

  const defaultGetPluginHeader = pluginName => {
    var _a;

    return (_a = cap.PluginHeaders) === null || _a === void 0 ? void 0 : _a.find(h => h.name === pluginName);
  };

  const getPluginHeader = ((_d = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _d === void 0 ? void 0 : _d.getPluginHeader) || defaultGetPluginHeader;

  const handleError = err => win.console.error(err);

  const pluginMethodNoop = (_target, prop, pluginName) => {
    return Promise.reject(`${pluginName} does not have an implementation of "${prop}".`);
  };

  const registeredPlugins = new Map();

  const defaultRegisterPlugin = (pluginName, jsImplementations = {}) => {
    const registeredPlugin = registeredPlugins.get(pluginName);

    if (registeredPlugin) {
      console.warn(`Capacitor plugin "${pluginName}" already registered. Cannot register plugins twice.`);
      return registeredPlugin.proxy;
    }

    const platform = getPlatform();
    const pluginHeader = getPluginHeader(pluginName);
    let jsImplementation;

    const loadPluginImplementation = /*#__PURE__*/function () {
      var _ref = (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
        if (!jsImplementation && platform in jsImplementations) {
          jsImplementation = typeof jsImplementations[platform] === 'function' ? jsImplementation = yield jsImplementations[platform]() : jsImplementation = jsImplementations[platform];
        } else if (capCustomPlatform !== null && !jsImplementation && 'web' in jsImplementations) {
          jsImplementation = typeof jsImplementations['web'] === 'function' ? jsImplementation = yield jsImplementations['web']() : jsImplementation = jsImplementations['web'];
        }

        return jsImplementation;
      });

      return function loadPluginImplementation() {
        return _ref.apply(this, arguments);
      };
    }();

    const createPluginMethod = (impl, prop) => {
      var _a, _b;

      if (pluginHeader) {
        const methodHeader = pluginHeader === null || pluginHeader === void 0 ? void 0 : pluginHeader.methods.find(m => prop === m.name);

        if (methodHeader) {
          if (methodHeader.rtype === 'promise') {
            return options => cap.nativePromise(pluginName, prop.toString(), options);
          } else {
            return (options, callback) => cap.nativeCallback(pluginName, prop.toString(), options, callback);
          }
        } else if (impl) {
          return (_a = impl[prop]) === null || _a === void 0 ? void 0 : _a.bind(impl);
        }
      } else if (impl) {
        return (_b = impl[prop]) === null || _b === void 0 ? void 0 : _b.bind(impl);
      } else {
        throw new CapacitorException(`"${pluginName}" plugin is not implemented on ${platform}`, ExceptionCode.Unimplemented);
      }
    };

    const createPluginMethodWrapper = prop => {
      let remove;

      const wrapper = (...args) => {
        const p = loadPluginImplementation().then(impl => {
          const fn = createPluginMethod(impl, prop);

          if (fn) {
            const p = fn(...args);
            remove = p === null || p === void 0 ? void 0 : p.remove;
            return p;
          } else {
            throw new CapacitorException(`"${pluginName}.${prop}()" is not implemented on ${platform}`, ExceptionCode.Unimplemented);
          }
        });

        if (prop === 'addListener') {
          p.remove = /*#__PURE__*/(0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
            return remove();
          });
        }

        return p;
      }; // Some flair ✨


      wrapper.toString = () => `${prop.toString()}() { [capacitor code] }`;

      Object.defineProperty(wrapper, 'name', {
        value: prop,
        writable: false,
        configurable: false
      });
      return wrapper;
    };

    const addListener = createPluginMethodWrapper('addListener');
    const removeListener = createPluginMethodWrapper('removeListener');

    const addListenerNative = (eventName, callback) => {
      const call = addListener({
        eventName
      }, callback);

      const remove = /*#__PURE__*/function () {
        var _ref3 = (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
          const callbackId = yield call;
          removeListener({
            eventName,
            callbackId
          }, callback);
        });

        return function remove() {
          return _ref3.apply(this, arguments);
        };
      }();

      const p = new Promise(resolve => call.then(() => resolve({
        remove
      })));
      p.remove = /*#__PURE__*/(0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
        console.warn(`Using addListener() without 'await' is deprecated.`);
        yield remove();
      });
      return p;
    };

    const proxy = new Proxy({}, {
      get(_, prop) {
        switch (prop) {
          // https://github.com/facebook/react/issues/20030
          case '$$typeof':
            return undefined;

          case 'toJSON':
            return () => ({});

          case 'addListener':
            return pluginHeader ? addListenerNative : addListener;

          case 'removeListener':
            return removeListener;

          default:
            return createPluginMethodWrapper(prop);
        }
      }

    });
    Plugins[pluginName] = proxy;
    registeredPlugins.set(pluginName, {
      name: pluginName,
      proxy,
      platforms: new Set([...Object.keys(jsImplementations), ...(pluginHeader ? [platform] : [])])
    });
    return proxy;
  };

  const registerPlugin = ((_e = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _e === void 0 ? void 0 : _e.registerPlugin) || defaultRegisterPlugin; // Add in convertFileSrc for web, it will already be available in native context

  if (!cap.convertFileSrc) {
    cap.convertFileSrc = filePath => filePath;
  }

  cap.getPlatform = getPlatform;
  cap.handleError = handleError;
  cap.isNativePlatform = isNativePlatform;
  cap.isPluginAvailable = isPluginAvailable;
  cap.pluginMethodNoop = pluginMethodNoop;
  cap.registerPlugin = registerPlugin;
  cap.Exception = CapacitorException;
  cap.DEBUG = !!cap.DEBUG;
  cap.isLoggingEnabled = !!cap.isLoggingEnabled; // Deprecated props

  cap.platform = cap.getPlatform();
  cap.isNative = cap.isNativePlatform();
  return cap;
};

const initCapacitorGlobal = win => win.Capacitor = createCapacitor(win);

const Capacitor = /*#__PURE__*/initCapacitorGlobal(typeof globalThis !== 'undefined' ? globalThis : typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : {});
const registerPlugin = Capacitor.registerPlugin;
/**
 * @deprecated Provided for backwards compatibility for Capacitor v2 plugins.
 * Capacitor v3 plugins should import the plugin directly. This "Plugins"
 * export is deprecated in v3, and will be removed in v4.
 */

const Plugins = Capacitor.Plugins;
/**
 * Provided for backwards compatibility. Use the registerPlugin() API
 * instead, and provide the web plugin as the "web" implmenetation.
 * For example
 *
 * export const Example = registerPlugin('Example', {
 *   web: () => import('./web').then(m => new m.Example())
 * })
 *
 * @deprecated Deprecated in v3, will be removed from v4.
 */

const registerWebPlugin = plugin => legacyRegisterWebPlugin(Capacitor, plugin);
/**
 * Base class web plugins should extend.
 */


class WebPlugin {
  constructor(config) {
    this.listeners = {};
    this.windowListeners = {};

    if (config) {
      // TODO: add link to upgrade guide
      console.warn(`Capacitor WebPlugin "${config.name}" config object was deprecated in v3 and will be removed in v4.`);
      this.config = config;
    }
  }

  addListener(eventName, listenerFunc) {
    var _this = this;

    const listeners = this.listeners[eventName];

    if (!listeners) {
      this.listeners[eventName] = [];
    }

    this.listeners[eventName].push(listenerFunc); // If we haven't added a window listener for this event and it requires one,
    // go ahead and add it

    const windowListener = this.windowListeners[eventName];

    if (windowListener && !windowListener.registered) {
      this.addWindowListener(windowListener);
    }

    const remove = /*#__PURE__*/function () {
      var _ref5 = (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
        return _this.removeListener(eventName, listenerFunc);
      });

      return function remove() {
        return _ref5.apply(this, arguments);
      };
    }();

    const p = Promise.resolve({
      remove
    });
    Object.defineProperty(p, 'remove', {
      value: function () {
        var _ref6 = (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
          console.warn(`Using addListener() without 'await' is deprecated.`);
          yield remove();
        });

        return function value() {
          return _ref6.apply(this, arguments);
        };
      }()
    });
    return p;
  }

  removeAllListeners() {
    var _this2 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this2.listeners = {};

      for (const listener in _this2.windowListeners) {
        _this2.removeWindowListener(_this2.windowListeners[listener]);
      }

      _this2.windowListeners = {};
    })();
  }

  notifyListeners(eventName, data) {
    const listeners = this.listeners[eventName];

    if (listeners) {
      listeners.forEach(listener => listener(data));
    }
  }

  hasListeners(eventName) {
    return !!this.listeners[eventName].length;
  }

  registerWindowListener(windowEventName, pluginEventName) {
    this.windowListeners[pluginEventName] = {
      registered: false,
      windowEventName,
      pluginEventName,
      handler: event => {
        this.notifyListeners(pluginEventName, event);
      }
    };
  }

  unimplemented(msg = 'not implemented') {
    return new Capacitor.Exception(msg, ExceptionCode.Unimplemented);
  }

  unavailable(msg = 'not available') {
    return new Capacitor.Exception(msg, ExceptionCode.Unavailable);
  }

  removeListener(eventName, listenerFunc) {
    var _this3 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const listeners = _this3.listeners[eventName];

      if (!listeners) {
        return;
      }

      const index = listeners.indexOf(listenerFunc);

      _this3.listeners[eventName].splice(index, 1); // If there are no more listeners for this type of event,
      // remove the window listener


      if (!_this3.listeners[eventName].length) {
        _this3.removeWindowListener(_this3.windowListeners[eventName]);
      }
    })();
  }

  addWindowListener(handle) {
    window.addEventListener(handle.windowEventName, handle.handler);
    handle.registered = true;
  }

  removeWindowListener(handle) {
    if (!handle) {
      return;
    }

    window.removeEventListener(handle.windowEventName, handle.handler);
    handle.registered = false;
  }

}

const WebView = /*#__PURE__*/registerPlugin('WebView');
/******** END WEB VIEW PLUGIN ********/

/******** COOKIES PLUGIN ********/

/**
 * Safely web encode a string value (inspired by js-cookie)
 * @param str The string value to encode
 */

const encode = str => encodeURIComponent(str).replace(/%(2[346B]|5E|60|7C)/g, decodeURIComponent).replace(/[()]/g, escape);

class CapacitorCookiesPluginWeb extends WebPlugin {
  setCookie(options) {
    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        // Safely Encoded Key/Value
        const encodedKey = encode(options.key);
        const encodedValue = encode(options.value); // Clean & sanitize options

        const expires = `; expires=${(options.expires || '').replace('expires=', '')}`; // Default is "; expires="

        const path = (options.path || '/').replace('path=', ''); // Default is "path=/"

        document.cookie = `${encodedKey}=${encodedValue || ''}${expires}; path=${path}`;
      } catch (error) {
        return Promise.reject(error);
      }
    })();
  }

  deleteCookie(options) {
    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        document.cookie = `${options.key}=; Max-Age=0`;
      } catch (error) {
        return Promise.reject(error);
      }
    })();
  }

  clearCookies() {
    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const cookies = document.cookie.split(';') || [];

        for (const cookie of cookies) {
          document.cookie = cookie.replace(/^ +/, '').replace(/=.*/, `=;expires=${new Date().toUTCString()};path=/`);
        }
      } catch (error) {
        return Promise.reject(error);
      }
    })();
  }

  clearAllCookies() {
    var _this4 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        yield _this4.clearCookies();
      } catch (error) {
        return Promise.reject(error);
      }
    })();
  }

}

const CapacitorCookies = registerPlugin('CapacitorCookies', {
  web: () => new CapacitorCookiesPluginWeb()
}); // UTILITY FUNCTIONS

/**
 * Read in a Blob value and return it as a base64 string
 * @param blob The blob value to convert to a base64 string
 */

const readBlobAsBase64 = /*#__PURE__*/function () {
  var _ref7 = (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = () => {
        const base64String = reader.result; // remove prefix "data:application/pdf;base64,"

        resolve(base64String.indexOf(',') >= 0 ? base64String.split(',')[1] : base64String);
      };

      reader.onerror = error => reject(error);

      reader.readAsDataURL(blob);
    });
  });

  return function readBlobAsBase64(_x) {
    return _ref7.apply(this, arguments);
  };
}();
/**
 * Normalize an HttpHeaders map by lowercasing all of the values
 * @param headers The HttpHeaders object to normalize
 */


const normalizeHttpHeaders = (headers = {}) => {
  const originalKeys = Object.keys(headers);
  const loweredKeys = Object.keys(headers).map(k => k.toLocaleLowerCase());
  const normalized = loweredKeys.reduce((acc, key, index) => {
    acc[key] = headers[originalKeys[index]];
    return acc;
  }, {});
  return normalized;
};
/**
 * Builds a string of url parameters that
 * @param params A map of url parameters
 * @param shouldEncode true if you should encodeURIComponent() the values (true by default)
 */


const buildUrlParams = (params, shouldEncode = true) => {
  if (!params) return null;
  const output = Object.entries(params).reduce((accumulator, entry) => {
    const [key, value] = entry;
    let encodedValue;
    let item;

    if (Array.isArray(value)) {
      item = '';
      value.forEach(str => {
        encodedValue = shouldEncode ? encodeURIComponent(str) : str;
        item += `${key}=${encodedValue}&`;
      }); // last character will always be "&" so slice it off

      item.slice(0, -1);
    } else {
      encodedValue = shouldEncode ? encodeURIComponent(value) : value;
      item = `${key}=${encodedValue}`;
    }

    return `${accumulator}&${item}`;
  }, ''); // Remove initial "&" from the reduce

  return output.substr(1);
};
/**
 * Build the RequestInit object based on the options passed into the initial request
 * @param options The Http plugin options
 * @param extra Any extra RequestInit values
 */


const buildRequestInit = (options, extra = {}) => {
  const output = Object.assign({
    method: options.method || 'GET',
    headers: options.headers
  }, extra); // Get the content-type

  const headers = normalizeHttpHeaders(options.headers);
  const type = headers['content-type'] || ''; // If body is already a string, then pass it through as-is.

  if (typeof options.data === 'string') {
    output.body = options.data;
  } // Build request initializers based off of content-type
  else if (type.includes('application/x-www-form-urlencoded')) {
    const params = new URLSearchParams();

    for (const [key, value] of Object.entries(options.data || {})) {
      params.set(key, value);
    }

    output.body = params.toString();
  } else if (type.includes('multipart/form-data')) {
    const form = new FormData();

    if (options.data instanceof FormData) {
      options.data.forEach((value, key) => {
        form.append(key, value);
      });
    } else {
      for (const key of Object.keys(options.data)) {
        form.append(key, options.data[key]);
      }
    }

    output.body = form;
    const headers = new Headers(output.headers);
    headers.delete('content-type'); // content-type will be set by `window.fetch` to includy boundary

    output.headers = headers;
  } else if (type.includes('application/json') || typeof options.data === 'object') {
    output.body = JSON.stringify(options.data);
  }

  return output;
}; // WEB IMPLEMENTATION


class CapacitorHttpPluginWeb extends WebPlugin {
  /**
   * Perform an Http request given a set of options
   * @param options Options to build the HTTP request
   */
  request(options) {
    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const requestInit = buildRequestInit(options, options.webFetchExtra);
      const urlParams = buildUrlParams(options.params, options.shouldEncodeUrlParams);
      const url = urlParams ? `${options.url}?${urlParams}` : options.url;
      const response = yield fetch(url, requestInit);
      const contentType = response.headers.get('content-type') || ''; // Default to 'text' responseType so no parsing happens

      let {
        responseType = 'text'
      } = response.ok ? options : {}; // If the response content-type is json, force the response to be json

      if (contentType.includes('application/json')) {
        responseType = 'json';
      }

      let data;
      let blob;

      switch (responseType) {
        case 'arraybuffer':
        case 'blob':
          blob = yield response.blob();
          data = yield readBlobAsBase64(blob);
          break;

        case 'json':
          data = yield response.json();
          break;

        case 'document':
        case 'text':
        default:
          data = yield response.text();
      } // Convert fetch headers to Capacitor HttpHeaders


      const headers = {};
      response.headers.forEach((value, key) => {
        headers[key] = value;
      });
      return {
        data,
        headers,
        status: response.status,
        url: response.url
      };
    })();
  }
  /**
   * Perform an Http GET request given a set of options
   * @param options Options to build the HTTP request
   */


  get(options) {
    var _this5 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this5.request(Object.assign(Object.assign({}, options), {
        method: 'GET'
      }));
    })();
  }
  /**
   * Perform an Http POST request given a set of options
   * @param options Options to build the HTTP request
   */


  post(options) {
    var _this6 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this6.request(Object.assign(Object.assign({}, options), {
        method: 'POST'
      }));
    })();
  }
  /**
   * Perform an Http PUT request given a set of options
   * @param options Options to build the HTTP request
   */


  put(options) {
    var _this7 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this7.request(Object.assign(Object.assign({}, options), {
        method: 'PUT'
      }));
    })();
  }
  /**
   * Perform an Http PATCH request given a set of options
   * @param options Options to build the HTTP request
   */


  patch(options) {
    var _this8 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this8.request(Object.assign(Object.assign({}, options), {
        method: 'PATCH'
      }));
    })();
  }
  /**
   * Perform an Http DELETE request given a set of options
   * @param options Options to build the HTTP request
   */


  delete(options) {
    var _this9 = this;

    return (0,c_Users_Personal_Documents_work_space_ionic_firebase_angular_appsocial_SocialDepo_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this9.request(Object.assign(Object.assign({}, options), {
        method: 'DELETE'
      }));
    })();
  }

}

const CapacitorHttp = registerPlugin('CapacitorHttp', {
  web: () => new CapacitorHttpPluginWeb()
});
/******** END HTTP PLUGIN ********/



/***/ })

}]);
//# sourceMappingURL=default-src_app_components_components_module_ts.js.map